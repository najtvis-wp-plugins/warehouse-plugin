<?php
/**
 * Plugin Name: Warehouse stocks
 * Plugin URI: https://najtvis.eu
 * Description: Plugin for warehouse
 * Version: 0.1
 * Author: Ondrej Nentvich
 * Author URI: https://najtvis.eu
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
define ( 'VERSION', '0.1');

include_once $_SERVER['DOCUMENT_ROOT'].'/wp-config.php';

// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}

if (!defined('WAREHOUSE_DIR')) {
    define('WAREHOUSE_DIR', plugin_dir_url( __FILE__ ));
}

if (!defined('WP_TEMP_DIR')) {
    define('WP_TEMP_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content/temp/');
}


if(session_id() == '')
	session_start(); 

// Wordpress admin menu
include_once  WAREHOUSE_DIR_SERVER."inc/admin/install.php";
include_once  WAREHOUSE_DIR_SERVER."inc/admin/uninstall.php";
include_once  WAREHOUSE_DIR_SERVER."inc/admin/plugin_menu.php";
include_once  WAREHOUSE_DIR_SERVER."inc/admin/privileges.php";

// Warehouse's shortcodes
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/_shortcodes.php";

function add_scripts() {
    wp_enqueue_style( 'dashicons' );
    wp_enqueue_style('warehouse_style', plugins_url('/css/styles.css', __FILE__));
    //wp_enqueue_script('warehouse_qr_scanner', plugins_url('/js/html5-qrcode.min.js', __FILE__));
    //wp_enqueue_script('conference_verify_input', plugins_url('/js/verify_input.js', __FILE__));
    //wp_enqueue_script('conference_functions', plugins_url('/js/functions.js', __FILE__));
}
add_action( 'wp_enqueue_scripts', 'add_scripts' );

//redirect to homepage when user is successfully logged in
add_filter( 'login_redirect', function( $url, $query, $user ) {
    return home_url();
}, 10, 3 );


?>