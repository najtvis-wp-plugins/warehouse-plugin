<?php

/**
 * Template Name: Import goods to the warehouse
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';

include_once  WAREHOUSE_DIR_SERVER."inc/common/getPartList.php";

function warehouse_create_multipart($parentID) { 
	global $wpdb;

	$current_user = wp_get_current_user();

	if(!isset($_SESSION['step_mp'])){
		$_SESSION['step_mp'] = 1;
	}

	$table_name = $wpdb->prefix.'warehouse_parts';
	$results = $wpdb->get_results("SELECT `partname` FROM $table_name WHERE `id`=".$parentID);
	if(!empty($results)){
		$editedPart = $results[0];
	}
	else{
		return -1;
	}


	if(isset($_POST['btnCheck']) || isset($_SESSION['txtListOfParts'])){
		if(isset($_POST['btnCheck']))
			$_SESSION['txtListOfParts'] = $_POST['txtListOfParts'];
			
		$decodedParts = array();
		$table_name = $wpdb->prefix.'warehouse_multipart';
		$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id_parent`=".$parentID);
		foreach($results as $listedPart){
			$table_name = $wpdb->prefix.'warehouse_parts';
			$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$listedPart->id_part);
			if(empty($results)){
				$where = array('id' => $listedPart->id);
				$table_name = $wpdb->prefix.'warehouse_multipart';
				$wpdb->delete($table_name, $where);
			}
			else{
				$part = $results[0];

				array_push($decodedParts, array('id'			=> $part->id, 
												'name' 			=> $part->partname, 
												'description'	=> $part->description, 
												'onStock'		=> $part->quantity, 
												'price'			=> $part->price, 
												'currency'		=> $part->currency,
												'replacement'	=> $part->replacement,
												'active'		=> $part->active,
												'quantity'		=> 0,
												'isListed'		=> $listedPart->quantity,
												'designator'	=> $listedPart->designator,
												'valid' 		=> 1							
				));
			}
		}
		
		$allValid = true;
		foreach(preg_split("/((\r?\n)|(\r\n?))/", $_SESSION['txtListOfParts']) as $line){
			if($line == "")
				continue;
			$fields = explode (";", $line);  
			
			//echo "<script>console.log( 'Debug Line: " . json_encode($fields) . "' );</script>";
			if(count($fields) >= 1){
				$table_name = $wpdb->prefix.'warehouse_parts';
				$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `partname`='".$fields[0]."'");

				if(!empty($results)){
					$part = $results[0];
					
					if(count($fields) >= 1){
						if(count($fields) == 1){
							$quantity = 0;
						}
						else{
							$quantity = $fields[1];
						}
						$x = array_search($part->partname, array_column($decodedParts, 'name'));
						
						if(count($fields) == 3){
							$designator = str_replace(" ", "", $fields[2] );
							$designator = str_replace(">", "", $designator );
							$designator = str_replace(",", ", ", $designator );
						}
						else
							$designator = "";

						if($x !== false){
							$decodedParts[$x]['quantity'] += $fields[1];
						}
						else{

							array_push($decodedParts, array('id'			=> $part->id, 
															'name' 			=> $part->partname, 
															'description'	=> $part->description, 
															'onStock'		=> $part->quantity, 
															'price'			=> $part->price, 
															'currency'		=> $part->currency,
															'active'		=> $part->active,
															'replacement'	=> $part->replacement,
															'quantity'		=> $quantity,
															'designator'	=> $designator, 
															'isListed'		=> 0,
															'valid' 		=> 1							
							));
						}
					}
				}
				else{
					$allValid = false;
					if(count($fields) == 3)
						array_push($decodedParts, array('name' 		=> $fields[0], 
														'quantity' 	=> $fields[1], 
														'designator'=> $fields[2], 
														'valid' 	=> 0
						));
					else if(count($fields) == 2)
						array_push($decodedParts, array('name' 		=> $fields[0], 
														'quantity' 	=> $fields[1], 
														'designator'=> "", 
														'valid' 	=> 0
						));
					else{
						array_push($decodedParts, array('name' 		=> $fields[0], 
														'quantity' 	=> "", 
														'designator'=> "", 
														'valid' 	=> 0
						));
					}
				}
			}
			else{
				$allValid = false;
				array_push($decodedParts, array('text' => $line, 'valid'=>-1));
			}
		} 
		
		$_SESSION['step_mp'] = 2;
		$_SESSION['decodedParts'] = $decodedParts;
		$_SESSION['noIDs'] = count($decodedParts);
	}
	
	if(isset($_POST['btnSave'])){
		$parts = $_SESSION['decodedParts'];
		$count = 0;
		
		$table_name = $wpdb->prefix.'warehouse_multipart';
		$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id_parent`=".$parentID);
		foreach($results as $part){
			$where = 	array('id' 	=> $part->id);
			$wpdb->delete( $table_name, $where);
		}
		

		for($i=0; $i<$_SESSION['noIDs']; ++$i){
			$text = 'quantity'.$i;
			$quantity = floatval($_POST[$text]);
			$text = 'id'.$i;
			$partID = $_POST[$text];
			$text = 'designator'.$i;
			$designator = $_POST[$text];
			$table_name = $wpdb->prefix.'warehouse_multipart';
			if($quantity > 0){
				$data = array(	
					'id_parent' 	=> $parentID,
					'id_part'		=> $partID, 
					'quantity'		=> $quantity,
					'designator'	=> $designator
				);
				$wpdb->insert($table_name, $data);
				++$count;
			}
		}
		$table_name = $wpdb->prefix.'warehouse_parts';
		if($count > 0){
			$data = array( 'multipart'	=> 1);

		}
		else{
			$data = array( 'multipart'	=> 0);
		}

		$where = 	array('id' 	=> $parentID);
		echo $wpdb->update( $table_name, $data, $where);

		$_SESSION['step_mp'] = 3;
		unset($_SESSION['decodedParts']);
		unset($_SESSION['noIDs']);
		unset($_SESSION['txtListOfParts']);
		unset($_SESSION['createMultipartID']);
	}
	
	if(isset($_POST['btnBack'])){
		--$_SESSION['step_mp'];
	}
	
?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<h1 class="stockCSS">Editace seznamu dílů součástky <?php echo $editedPart->partname; ?></h1>
			<?php 
			switch($_SESSION['step_mp']){
				default:
				case 1:
					$_SESSION['step_mp'] = 1;
			?>
				<div align='center'>
					<button class="tablinkSel" id="scan" style="float: center;">Skenovat</button>
					<button class="tablink" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				<div>
					<form action="" method="post" class="stockCSS">
						<div class="one_column">
							<label for="txtListOfParts" id="name_label">Naskenované součástky a jejich množství (cena a měna):</label>
							<textarea id="txtListOfParts" rows="4" cols="50" name="txtListOfParts" autofocus><?php if(isset($_SESSION['txtListOfParts'])) echo $_SESSION['txtListOfParts']; ?></textarea>
						</div>
						<div>
							<b>Format:</b> Barcode;Quantity;Designator
						</div>
						<div class="one_column">
							<button name="btnCheck" type="submit" style="float: right;">Načíst</button>
						</div>
					</form>
				</div>
			<?php
				break;


				case 2:
			?>

				<script>
                    var lastID = <?php echo count($decodedParts); ?>;
                    function deleteRow(row) {
                        var i = row.parentNode.parentNode.rowIndex;
                        document.getElementById('partsTable').deleteRow(i);
                    }

                    function addRow() {
                        var tableID = "partsTable";
                        var table = document.getElementById(tableID);
                        var rowCount = table.rows.length;

                        var row = table.insertRow(rowCount);
                        var newcell = row.insertCell(0);
                        newcell.innerHTML = "<input id='id1"+lastID+"' name='part"+lastID+"' type='text' style='width: 100%;' required onchange='getPossibleParts(this.value, this.id)'></input>";
                       
						newcell = row.insertCell(1);
                        newcell.innerHTML = "";

						newcell = row.insertCell(2);
                        newcell.innerHTML = "<input required autofocus type='number' min='0' step='0.01' name='quantity"+lastID+"' id='idQTY"+lastID+"' value=''>";
                        
						newcell = row.insertCell(3);
                        newcell.innerHTML = "";
                        
						newcell = row.insertCell(4);
                        newcell.innerHTML = "<span name='desc"+lastID+"' id='desc"+lastID+"'> </span>";

						newcell = row.insertCell(5);
                        newcell.innerHTML = "<input required autofocus type='text' name='designator"+lastID+"' id='designatorID"+lastID+"' value=''>";

						newcell = row.insertCell(6);
						newcell.align = "center";
                        newcell.innerHTML = "<a class='dashicons dashicons-trash' onclick='deleteRow(this)'/>";

                        ++lastID;
                    }

					function getPossibleParts(str, id) {

						jQuery('#newCustomerForm').submit(ajaxSubmit); 
                        
                        function ajaxSubmit(){
                            
                            var newCustomerForm = jQuery(this).serialize();
                            
                            jQuery.ajax({
                                type:"POST",
                                url: "/wp-admin/admin-ajax.php",
                                data: newCustomerForm,
                                success:function(data){
                                    jQuery("#feedback").html(data);
                                },
                                error: function(errorThrown){
                                    alert(errorThrown);
                                }   
                            });
                            
                            return false;
                        }
						
						if (str == "") {
							document.getElementById(id).innerHTML = "";
							return;
						}
						const xhttp = new XMLHttpRequest();
						xhttp.onload = function() {
							document.getElementById(id).innerHTML = this.responseText;
						}
						xhttp.open("GET", "https://warehouse.microced.cz/wp-content/plugins/warehouse-plugin/inc/common/getPartList.php?pn="+str);
						xhttp.send();
					}
                </script>

				<div align='center'>
					<button class="tablink" id="scan" style="float: center;">Skenovat</button>
					<button class="tablinkSel" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				
				<form action="" method="post" class="stockCSS">
					<hr />
					<div></div>
					<table class="warehouse" id="partsTable">
					<tr class='headerRow'>
						<th>Název</th>
						<th class='thCenter'>Skladem</th>
						<th>Množství<font color="red">*</font></th>
						<th>Před</th>
						<th>Popis</th>
						<th>Cena</th>
						<th>Designator</th>
					</tr>
					<?php
						$id = 0;
						foreach($decodedParts as $part){	
								if($part['valid'] == 0){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td>".$part['name']."</td>";
									echo "<td></td>";
									echo "<td>".$part['quantity']."</td>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td></td>";
									if(isset($part['designator']))
										echo "<td>".$part['designator']."</td>";
									else	
									echo "<td></td>";
								}
								else if($part['valid'] == -1){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"6\">".$part['text']."</td>";
								}
								else{
									if(!isset($part['price']))
										$part['price'] = "";
									if(!isset($part['currency']))
										$part['currency'] = "";

									echo "<tr>";
									echo "<td>";
									echo "<span class='warehouseItem'>";
									echo '<input type="radio" id="part1'.$id.'" name="id'.$id.'" value="'.$part['id'].'" checked/>';
									echo '<label for="part1'.$id.'">'.$part['name'].'</label> ';
									echo "</span>";	

									// add partName
									if($part['replacement'] != ""){
										$replacementPartsList = explode (";", $part['replacement']);
										$i=2;
										foreach($replacementPartsList as $listedPart){ 
											$table_name = $wpdb->prefix.'warehouse_parts';
											$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `partname`='".$listedPart."'");
											
											if(!empty($results)){
												$replacementPart = $results[0];				
												echo "<span class='warehouseItem'>";
												echo '<input type="radio" id="partx'.$i.$id.'" name="id'.$id.'" value="'.$replacementPart->id.'"/>';
												echo '<label for="partx'.$i.$id.'">'.$replacementPart->partname.'</label>';
												echo "</span>";	
											}
										}
									}								
									
									echo "</td>";
									echo "<td align='center'>".$part['onStock'];
									if($part['replacement'] != ""){
										foreach($replacementPartsList as $listedPart){ 
											$table_name = $wpdb->prefix.'warehouse_parts';
											$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `partname`='".$listedPart."'");
											
											if(!empty($results)){
												$replacementPart = $results[0];		
												echo "<br/>".$replacementPart->quantity;
											}
										}
									}
									echo "</td>";

									// add quantity
									if($part['quantity'] > 0){
										echo "<td><input required autofocus type='number' min='0' step='0.01' name='quantity".$id."'  value='".floatval($part['quantity'])."'></td>";									
									}
									else{
										echo "<td><input required autofocus type='number' min='0' step='0.01' name='quantity".$id."'  value='".floatval($part['isListed'])."'></td>";									
									}

									echo "<td align='center'>".$part['isListed']."</td>";
									echo "<td>".$part['description'];
									if($part['replacement'] != ""){
										foreach($replacementPartsList as $listedPart){ 
											$table_name = $wpdb->prefix.'warehouse_parts';
											$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `partname`='".$listedPart."'");
											
											if(!empty($results)){
												$replacementPart = $results[0];		
												echo "<br/>".$replacementPart->description;
											}
										}
									}
									echo "</td>";
									echo "<td align='center'>".$part['price']." ".$part['currency'];
									if($part['replacement'] != ""){
										foreach($replacementPartsList as $listedPart){ 
											$table_name = $wpdb->prefix.'warehouse_parts';
											$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `partname`='".$listedPart."'");
											
											if(!empty($results)){
												$replacementPart = $results[0];		
												echo "<br/>".$replacementPart->price." ".$replacementPart->currency;
											}
										}
									}
									echo "</td>";
									echo "<td><input required type='text' name='designator".$id."'  value='".$part['designator']."'></td>";									
								}
								echo "</tr>";
							echo "</form>";
							++$id;
						}
					?>
					
					</table>
					
					<center>
						<button  id="addPartTypeRow" onClick="addRow()">Add row</button>
					</center>

					</br>

					<div class="one_column">
						<script>
							function removeRequired(form){
								$.each(form, function(key, value) {
									if ( value.hasAttribute("required")){
										value.removeAttribute("required");
									}
								});
							}
						</script>
						
						<?php if($allValid == true)
							echo "<button name='btnSave' type='submit' style='float: right;'>Uložit</button>";
						?>	

						

						<button name="btnBack" id="btnBack" style="float: left; " onClick="removeRequired(this.form)">Zpět</button>
					</div>
				</form>
			
			<?php
				break;
				case 3:
			?>
					<div align='center'>
						<button class="tablink" id="scan" style="float: center;">Skenovat</button>
						<button class="tablink" id="check" style="float: center;">Kontrola</button>
						<button class="tablinkSel" id="save" style="float: center;">Uložit</button>
					</div>
					
					<h2 class="stockCSS"><font color="green">Záznam uložen</font></h2>
					
					<form action="" method="post" class="stockCSS">
						<div class="one_column">
							<button name="btnNew" type="submit" style="float: right;">Nový záznam</button>
						</div>
					</form>
			<?php
				unset($_SESSION['step_mp']);
				break;
			}
			?>


</div><!-- .wrap -->

<?php
}
