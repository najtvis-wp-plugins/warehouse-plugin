<?php


include_once  WAREHOUSE_DIR_SERVER."inc/common/exportPDF2LaTeX.php";

function updateSettingsValue($name, $value){
	global $wpdb;

	$table_name = $wpdb->prefix.'warehouse_settings';
	$results = $wpdb->get_results("SELECT * FROM $table_name WHERE name='$name'");
	if(count($results) > 0){
		$data  = array( 'value'		=> $value);
		$where = array( 'id' 		=> $results[0]->id);
		$wpdb->update($table_name, $data, $where);
		return 1;
	}
	else{
		$data  = array( 'name'		=> $name, 
						'value'		=> $value);
		$wpdb->insert($table_name, $data);
		return -1;
	}
}

function createSettingsValue($name, $value = ""){
	global $wpdb;

	$table_name = $wpdb->prefix.'warehouse_settings';
	$results = $wpdb->get_results("SELECT * FROM $table_name WHERE name='$name'");
	if(empty($results)){
		$data  = array( 'name'		=> $name, 
						'value'		=> $value);
		$wpdb->insert($table_name, $data);
		return 1;
	}
    return 0;
}

function getSettingsValue($name){
	global $wpdb;
	
	$table_name = $wpdb->prefix.'warehouse_settings';
	$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `name`='$name'");
	if(empty($results))
		return 0;
	else
		return $results[0]->value;	
}

function insertMD5hash($rowID, $database, $md5){
	global $wpdb;

	$table_name = $wpdb->prefix.'warehouse_hashes';
    $data  = array( 'rowID'		=> $rowID, 
					'db'		=> $database, 
					'md5'		=> $md5
				  );
    $wpdb->insert($table_name, $data);
    return 1;
}

function findMD5hash($rowID, $database, $md5){
	global $wpdb;

	$table_name = $wpdb->prefix.'warehouse_hashes';
	$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `rowID`=".$rowID." AND `db`='".$database."' AND `md5`='".$md5."'");
    if(empty($results))
		return 0;
	else
    	return 1;
}

function deleteMD5hash($rowID, $database, $md5){
	global $wpdb;

	$table_name = $wpdb->prefix.'warehouse_hashes';
	$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `rowID`=".$rowID." AND `db`='".$database."' AND `md5`='".$md5."'");
    if(!empty($results)){
		$where = 	array('id' 			=> $results[0]->id);
		$wpdb->delete( $table_name, $where);
	}
}

function roundUpStep(int $number, $step)
{
    return round(($number+$step/2)/$step)*$step;
}

function normalizeString ($str = ''){
    $str = strip_tags($str); 
    $str = str_replace(' ', '_', $str);
    $str = preg_replace('/[\r\n\t ]+/', '', $str);
    $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', '', $str);
    $str = strtolower($str);
    $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
    $str = htmlentities($str, ENT_QUOTES, "utf-8");
    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
    $str = rawurlencode($str);
    $str = str_replace('%', '', $str);
    return $str;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateMD5($input = ""){
	if($input == ""){
		$input = date("YmdHis").generateRandomString(10);
		return md5($input);
	}
}

function addMultiPart($partID, $quantity, $level=0){
	global $wpdb;

	// get current quantity of the goods
	$table_name = $wpdb->prefix.'warehouse_parts';
	$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$partID);
	if(empty($results)){
		return NULL;
	}
	$addedPartTop = $results[0];
	if($addedPartTop->multipart && $level <= 10){
		// read list of parts
		$table_name = $wpdb->prefix.'warehouse_multipart';
		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id_parent`=".$addedPartTop->id);
		$parts = $results;
		$partsCollection = [];
		//echo "multipart: ".$addedPartTop->partname."<br>";
		foreach($parts as $part){
			$loadedMultiPart = addMultiPart($part->id_part, $part->quantity * $quantity, $level+1);
			if(!is_null($loadedMultiPart))
				$partsCollection[] = $loadedMultiPart;
		}

		// sort loaded components accordint to type and then partname
		// $type  = array_column($partsCollection, 'type');
		$type = array_map('strtolower', array_column($partsCollection, 'type'));
		$partname = array_map('strtolower', array_column($partsCollection, 'partname'));
		array_multisort($type, SORT_ASC, $partname, SORT_ASC, $partsCollection);	
		// array_multisort($partname, SORT_ASC, $partsCollection);	

		return array( 	'id'			=>  $addedPartTop->id,
						'partname'		=>	$addedPartTop->partname,
						'sku'			=>  $addedPartTop->sku,
						'qty'			=>	$quantity,
						'warehouseQTY'	=>	$addedPartTop->quantity,
						'price'			=>	$addedPartTop->price * $quantity,
						'currency'		=>	$addedPartTop->currency,
						'type'			=>	$addedPartTop->type,
						'description'	=>	$addedPartTop->description,
						'multipart'		=>  $addedPartTop->multipart,
						'pdf'			=>  $addedPartTop->pdf,
						'rohs'			=>  $addedPartTop->rohs,
						'image'			=>  $addedPartTop->image,
						'barcode'		=>  $addedPartTop->barcode,
						'manufacturer'	=>	$addedPartTop->manufacturer,
						'leadTime'		=>	$addedPartTop->leadTime,
						'moq'			=>	$addedPartTop->moq,
						'quantityPerBox'=>	$addedPartTop->quantityPerBox,
						'currentSupplier'	=>	$addedPartTop->currentSupplier,
						'details'		=>	$addedPartTop->details,
						'datasheet'		=>	$addedPartTop->pdf,
						'terminal'		=>	false,
						'childs'		=>	$partsCollection
		);
	}
	else{
		//echo "  part: ".$addedPartTop->partname."<br>";
		return array( 	'id'			=>  $addedPartTop->id,
						'partname'		=>	$addedPartTop->partname,
						'sku'			=>	$addedPartTop->sku,
						'qty'			=>	$quantity,
						'warehouseQTY'	=>	$addedPartTop->quantity,
						'price'			=>	$addedPartTop->price * $quantity,
						'currency'		=>	$addedPartTop->currency,
						'type'			=>	$addedPartTop->type,
						'description'	=>	$addedPartTop->description,
						'multipart'		=>  $addedPartTop->multipart,
						'pdf'			=>  $addedPartTop->pdf,
						'rohs'			=>  $addedPartTop->rohs,
						'image'			=>  $addedPartTop->image,
						'barcode'		=>  $addedPartTop->barcode,
						'manufacturer'	=>	$addedPartTop->manufacturer,
						'leadTime'		=>	$addedPartTop->leadTime,
						'moq'			=>	$addedPartTop->moq,
						'quantityPerBox'=>	$addedPartTop->quantityPerBox,
						'currentSupplier'	=>	$addedPartTop->currentSupplier,
						'details'		=>	$addedPartTop->details,
						'datasheet'		=>	$addedPartTop->pdf,
						'terminal'		=>	true,
						'childs'		=>	[]
		);
	}
}


function calculateTotalPartPrices($parts, $indentLevel=0, $detailed=1){

/*	if($indentLevel == 0){
		$listOfParts = addMultiPart($partID, 1);
	}
*/
	$totalPrice = [];
	$warehouseUrl = get_permalink( get_page_by_path( 'warehouse' ));
	
	foreach ($parts as $part){

		if(!$part['terminal'] && ($detailed || $indentLevel == 0) ){
			$prices = calculateTotalPartPrices($part['childs'], $indentLevel+1, $detailed);
			foreach($prices as $curr => $price){
				if(isset($totalPrice[$curr]))
					$totalPrice[$curr] += $price;
				else
					$totalPrice[$curr] = $price;
			}
		}
		else{
			if(!isset($totalPrice[$part['currency']])){
				$totalPrice[$part['currency']] = 0;
				settype($totalPrice[$part['currency']], 'double');
				$totalPrice[$part['currency']] = (double) $part['price'];
			}
			else
				$totalPrice[$part['currency']] += (double) $part['price'];
		}
	}
	return $totalPrice;
}

function calculateTotalPartPrice($partID){
	$listOfParts = addMultiPart($partID, 1);
	$totalPrice = calculateTotalPartPrices(array( 0 => $listOfParts), 0, 1);

	$sumHomeCurrency = 0;
	$homeCurrency = getSettingsValue("CURRENCY_HOME");

	foreach($totalPrice as $curr => $price){
		if($curr == $homeCurrency){
			$sumHomeCurrency += (double) $price;
		}
		else{
			$currRatio = getSettingsValue("CONV;".$curr.";".$homeCurrency);
			if(!is_null($currRatio))
				$sumHomeCurrency += $price * (double) floatval($currRatio);
		}
	}
	return $sumHomeCurrency;

}

function showMultiParts($parts, $indentLevel, $detailed, $simplified = 0, $subOnStock = 0, $addSKU = 0, $showImage = 0){
	$indent = $indentLevel*20;
	
	$warehouseUrl = get_permalink( get_page_by_path( 'warehouse' ));
	$homeCurrency = getSettingsValue("CURRENCY_HOME");

	if($simplified == 0){
		if($indentLevel == 0){
			echo "<table class='warehouse'>";
			echo 	"<tr>";
			if($showImage == 1){
				echo "<th>IMG</th>";
			}
			echo 		"<th>Partname <span style='float: right;'>Info</span></th>";
			echo 		"<th>Type</th>";
			echo 		"<th>Description</th>";
			echo 		"<th>Quantity</th>";
			echo 		"<th>Price</th>";
			echo 		"<th>Total price</th>";
			echo 	"<tr>";
		}
		foreach ($parts as $part){
			if($part['terminal'])
				echo "<tr>";
			else
				echo "<tr style='background-color:#eeeeee'>";

			
			if($showImage == 1){
				echo "<td>";
				if($part['image'] != "")
					echo '<img src="'.$part['image'].'" style="height:100px;"> ';
				
					echo "</td>";
			}
			

			
			
			echo 	"<td><b style='padding-left: ".$indent."px'><a href='".$warehouseUrl."?detailID=".$part['id']."'>".$part['partname']."</a>"."</b>";

			echo    "<span style='float: right;'>";
			if($part['pdf'] != "")
				echo " <a class='dashicons dashicons-pdf' href='".$part['pdf']."'/> ";
			
			if($part['image'] != "")
				echo " <a class='dashicons dashicons-format-image' href='".$part['image']."'/> ";
			
			if($part['rohs'] != "")
				echo " <a class='dashicons dashicons-format-aside' style='color: #2AA243;' href='".$part['rohs']."'/> ";

			if($part['multipart']){
				$url = get_permalink( get_page_by_path( 'warehouse/list_of_multiparts' ));
				echo " <a class='dashicons dashicons-editor-ul' href='".$url."?part_id=".$part['id']."'/> ";
			}
			echo    "</span>";
			echo 	"</td>";

			echo 	"<td align='center'>".$part['type']."</td>";
			echo    "<td>";
			echo 	" ".$part['description'];
			echo 	"</td>";
			echo 	"<td align='center'>".$part['qty']."</td>";
			
			$partPrice = calculateTotalPartPrice($part['id']);
			if($part['multipart'] || $indentLevel == 0){
				echo 	"<td align='right'><font color='#c7c7c7'>".round(calculateTotalPartPrice($part['id']), 3)." ".$homeCurrency."</font></td>";
			}
			else{
				echo 	"<td align='right'>".round(calculateTotalPartPrice($part['id']),3)." ".$part['currency']."</td>";
			}

			$partPrice = calculateTotalPartPrice($part['id']) * $part['qty'];
			if($part['multipart'] || $indentLevel == 0){
				echo 	"<td align='right'><font color='#c7c7c7'>".round($partPrice, 3)." ".$homeCurrency."</font></td>";
			}
			else{
				echo 	"<td align='right'>".round($part['price'],3)." ".$part['currency']."</td>";
			}
			echo "</tr>";

			if(!$part['terminal'] && ($detailed || $indentLevel == 0) ){
				$prices = showMultiParts($part['childs'], $indentLevel+1, $detailed, $simplified, $subOnStock, $addSKU, $showImage);
				foreach($prices as $curr => $price){
					if(isset($totalPrice[$curr]))
						$totalPrice[$curr] += $price;
					else
						$totalPrice[$curr] = $price;
				}
			}
			else{
				if($part['multipart']){
					if(!isset($totalPrice[$homeCurrency]))
						$totalPrice[$homeCurrency] = $partPrice;
					else
						$totalPrice[$homeCurrency] += $partPrice;
				}
				else{
					if(!isset($totalPrice[$part['currency']]))
						$totalPrice[$part['currency']] = $part['price'];
					else
						$totalPrice[$part['currency']] += $part['price'];
				}
			}
		}
	}
	else{	// detailed for ordering
		if($indentLevel == 0){
		echo "<table style='margin:5%;width:90%'>";
		}
		foreach ($parts as $part){
			if($part['terminal'] || (!$part['terminal'] && $indentLevel <=1 )){
				echo "<tr>";
				echo 	"<td><b style='padding-left: ".$indent."px'>";
				if($addSKU)
					echo $part['sku'].";";
				echo       $part['partname'].";".$part['qty']."</td>";
				echo "</tr>";
			}

			if(!$part['terminal'] && ($detailed || $indentLevel == 0) ){
				$prices = showMultiParts($part['childs'], $indentLevel+1, $detailed, $simplified, $subOnStock, $addSKU);
			}
		}
		$totalPrice = 0;
	}
	
	if($indentLevel == 0){
		$sumHomeCurrency = 0;
		$homeCurrency = getSettingsValue("CURRENCY_HOME");

		if($simplified == 0 ){
			foreach($prices as $curr => $price){
				echo "<tr><td><b>Price in ".$curr."</td><td></td><td>" ;
				if($curr == $homeCurrency){
					$sumHomeCurrency += $price;
				}
				else{
					$currRatio = getSettingsValue("CONV;".$curr.";".$homeCurrency);
					$sumHomeCurrency += $price * floatval($currRatio);
					if($currRatio > 0)
						$invRatio = 1/$currRatio;
					else
						$invRatio = -1;
					echo "Conversion rate 1 ".$curr." = ".$currRatio." ".$homeCurrency." | 1 ".$homeCurrency." = ".round($invRatio, 3)." ".$curr;
				}
				echo "</td><td></td><td></td><td align='right'><b>".round($price,3)." ".$curr."</b></td></tr>";
			}
		
			echo "<tr><td><b><font color='red'>Total price in ".$homeCurrency."</font></b></td><td></td><td></td><td></td><td></td><td align='right'><font color='red'><b>".round($sumHomeCurrency,3)." ".$homeCurrency."</b></font></td></tr>";
			echo "</table>";
		}
	}
	return $totalPrice;
}

function addSKUNumber($partID, $skuPrefix){
	global $wpdb;
	$table_name = $wpdb->prefix.'warehouse_parts';
	$lastPrefixValue = strval(getSettingsValue("LAST_SKU_PREFIX_VAL_".$skuPrefix));
	$prefixLen = strval(getSettingsValue("SKU_PREFIX_LEN"));
	
	do{
		$skuNumber = str_pad($lastPrefixValue++, $prefixLen, '0', STR_PAD_LEFT);
		$sku = $skuPrefix.$skuNumber;
		$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `sku`='".$sku."' ORDER BY `type` ASC, `partname` ASC");
	}
	while(!empty($res));

	$data = array('sku' => $sku);
	$where = array('id' => $partID);
	$wpdb->update($table_name, $data, $where);
	updateSettingsValue("LAST_SKU_PREFIX_VAL_".$skuPrefix, $lastPrefixValue);
}

function exportPartToPDF($partID){
	global $wpdb;
	
	return exportPartToPDF_LaTeX($partID);
}

function viewPartDetails($partID){
	global $wpdb;
	
	$table_name = $wpdb->prefix.'warehouse_parts';
	$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$partID);
	$part = $results[0];

	if(isset($_POST['exportInPDF'])){
		$outputExport2PDF = exportPartToPDF($_POST['exportInPDF']);
	}

	if($part->multipart){
		$listOfParts = addMultiPart($part->id, 1);
	}
	$editPartUrl = get_permalink( get_page_by_path( 'warehouse/edit_component' ));
	?>
	<h2>Detail součástky:</h2>
	
	<div class="one_column" style="float: right;">
		<!-- <span  style='float: right;'> -->
			<form action="" method="post" class='stockCSS'>
				<?php echo " <button type='submit'  name='exportInPDF' value='".$partID."'><span class='dashicons dashicons-pdf' > </span></button>"; ?>
			</form>
			<?php
				if(isset($outputExport2PDF))
					echo $outputExport2PDF;
			?>
		<!-- </span> -->
		<span  style='float: right;'>

			<form action="<?php echo $editPartUrl; ?>" method="post" class='stockCSS'> 
			<?php
				$current_user = wp_get_current_user();
				
				$can_edit = get_user_meta( $current_user->ID, 'warehouse_can_edit', true);

				if($can_edit == '1'){
					if($part->active)
						echo " <button type='submit' name='editPart' value='".$partID."'><span class='dashicons dashicons-info'> </span></button>";
					else
						echo " <button type='submit' name='editPart' value='".$partID."'><span class='dashicons dashicons-info' style='color: red;'> </span></button>";
					
					if($part->pdf == "")
						echo " <button type='submit' name='addPDF' value='".$partID."'><span class='dashicons dashicons-media-document'> </span></button>";
					else
						echo " <button type='submit'  name='addPDF' value='".$partID."'><span class='dashicons dashicons-media-document' style='color: #00ff00;'> </span></button>";

					if($part->rohs == "")
						echo " <button type='submit' name='addROHS' value='".$partID."'>ROHS</button>";
					else
						echo " <button type='submit'  name='addROHS' value='".$partID."'><span style='color: #00ff00;'>ROHS</span></button>";

					if($part->image == "")
						echo " <button type='submit'  name='addIMG' value='".$partID."'><span class='dashicons dashicons-format-image'> </span></button>";
					else
						echo " <button type='submit'  name='addIMG' value='".$partID."'><span class='dashicons dashicons-format-image' style='color: #00ff00;'> </span></button>";

					if($part->multipart)
						echo " <button type='submit'  name='addMultiParts' value='".$partID."'><span class='dashicons dashicons-editor-ul' style='color: #00ff00;'> </span></button>";
					else
						echo " <button type='submit'  name='addMultiParts' value='".$partID."'><span class='dashicons dashicons-editor-ul' > </span></button> ";
					
					
				}

				$is_admin = get_user_meta( $current_user->ID, 'warehouse_is_admin', true);
				if($is_admin == '1')
					echo " <button type='submit'  name='changeQauntity' value='".$part->id."'><span class='dashicons dashicons-database-view'></span></button> ";
				
				?>		
			</form> 
		<!-- </span> -->
	</div>
	<table>
		<tr>
			<th align='left'>Název součástky: </th>
			<td><?php echo strtoupper($part->partname) ?></td>
		</tr>
		<tr>
			<th align='left'>Náhrada:</th>
			<td><?php echo $part->replacement ?></td>
		</tr>
		<tr>
			<th align='left'>Čárový kód: </th>
			<td><?php echo strtoupper($part->barcode) ?></td>
		</tr>
		<tr>
			<th align='left'>SKU:</th>
			<td><?php echo $part->sku ?></td>
		</tr>
		<tr>
			<th align='left'>Typ:</th>
			<td><?php echo $part->type ?></td>
		</tr>
		<tr>
			<th align='left'>Krátký popis:</th>
			<td><?php echo $part->description ?></td>
		</tr>
		<tr>
			<th align='left'>Výrobce:</th>
			<td><?php echo $part->manufacturer ?></td>
		</tr>
		<tr>
			<th align='left'>Pouzdro:</th>
			<td><?php echo $part->package ?></td>
		</tr>
		<tr>
			<th align='left'>Detailní popis:</th>
			<td><?php echo $part->details ?></td>
		</tr>
	</table>

	<h2>Skladové info:</h2>
	<table>
		<tr>
			<th align='left'>Množství:</th>
			<td><?php echo $part->quantity ?></td>
		</tr>
		<tr>
			<th align='left'>Pozice ve skladu:</th>
			<td><font color='".<?php echo $part->posColour ?>"'><?php echo $part->position ?></font></td>
		</tr>
		<tr>
			<th align='left'>Typ balení:</th>
			<td><?php echo $part->boxPackageType ?></td>
		</tr>
		<tr>
			<th align='left'>Aktivní položka:</th> 
			<td>
			<?php
			if($part->active)
				echo '<span class="dashicons dashicons-yes" style="color: green;"></span>';
			else
				echo '<span class="dashicons dashicons-no" style="color: red;"></span>';
			?>			
			</td>
		</tr>
	</table>

	<h2>Info pro nákup:</h2>
	<table>
		<tr>
			<th align='left'>Množství v balení:</th>
			<td><?php echo $part->quantityPerBox ?></td>
		</tr>
		<tr>
			<th align='left'>Minimální množství:</th>
			<td><?php echo $part->minQuantity ?></td>
		</tr>
		<tr>
			<th align='left'>Doba dodání (dní):</th>
			<td><?php echo $part->leadTime ?></td>
		</tr>
		<tr>
			<th align='left'>Cena:</th>
			<?php
			if($part->multipart){
				$homeCurrency = getSettingsValue("CURRENCY_HOME");
				$partPrice = calculateTotalPartPrice($part->id);
				
				echo "<td>".round($partPrice, 3)." ".$homeCurrency."</td>";
			}
			else{
				echo "<td>".$part->price." ".$part->currency."</td>";
			}
			?>
		</tr>
		<tr>
			<th align='left'>Dodavatel:</th>
			<td><?php echo $part->currentSupplier ?></td>
		</tr>

	</table>			
		
	<?php
		if($part->multipart){
			echo "<h2>Seznam Součástek</h2>";
			showMultiParts(array( 0 => $listOfParts), 0, 0, 0, 0, 0, 1);
		}
}

function deleteDir($path) {
    if (empty($path)) { 
        return false;
    }
    return is_file($path) ?
            @unlink($path) :
            array_map(__FUNCTION__, glob($path.'/*')) == @rmdir($path);
}

?>