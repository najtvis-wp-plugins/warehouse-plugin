<?php

/**
 * Template Name: Cart with components to order
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}

include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/manufacture.php";
include_once  WAREHOUSE_DIR_SERVER."inc/common/functions.php";

function exportCSV(){
    global $wpdb;
    global $requiredComponents;

    ob_start();



    $array = [];
    switch($_POST['exportCSV']){
        case 'exportParts':
            $fh = @fopen( 'php://output', 'w' );
            fprintf( $fh, chr(0xEF) . chr(0xBB) . chr(0xBF) );
            header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
            header( 'Content-Description: File Transfer' );
            header( 'Content-type: text/csv' );
            header( "Content-Disposition: attachment; filename={$_POST['CSVfilename']}" );
            header( 'Expires: 0' );
            header( 'Pragma: public' );

            $table_name = $wpdb->prefix.'warehouse_parts';
            $results = $wpdb->get_results("SELECT * FROM $table_name ORDER BY `type` ASC, `partname` ASC");
            $parts = $results;
            $x = array( 'partname', 
                        'description',    
                        'details',
                        'manufacturer',  
                        'sku', 
                        'type', 
                        'quantity', 
                        'quantityPerBox', 
                        'boxPackageType',   
                        'price', 
                        'currency', 
                        'position',    
                        'currentSupplier',    
                        'leadTime',    
                        'moq'              
            );
            array_push($array, $x);

            foreach($parts as $part){
                $price = $part->price;
                $currency = $part->currency;
                if($part->multipart){
                    $price = calculateTotalPartPrice($part->id);
                    $currency = getSettingsValue("CURRENCY_HOME");
                }
                $x = array( $part->partname, 
                            $part->description,    
                            $part->details,
                            $part->manufacturer,  
                            $part->sku, 
                            $part->type, 
                            $part->quantity, 
                            $part->quantityPerBox, 
                            $part->boxPackageType,   
                            $price, 
                            $currency, 
                            $part->position,    
                            $part->currentSupplier,    
                            $part->leadTime,    
                            $part->moq              
                );
                array_push($array, $x);
            }

            foreach($array as $line){
                fputcsv($fh, $line, ','); 
            }
        
            fclose( $fh );
            ob_end_flush();

            break;

        case 'exportManufacture':

            $fh = @fopen( 'php://output', 'w' );
            fprintf( $fh, chr(0xEF) . chr(0xBB) . chr(0xBF) );
            header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
            header( 'Content-Description: File Transfer' );
            header( 'Content-type: text/csv' );
            header( "Content-Disposition: attachment; filename={$_POST['CSVfilename']}" );
            header( 'Expires: 0' );
            header( 'Pragma: public' );

            // read list of parts
            $table_name = $wpdb->prefix.'warehouse_manufacture_plan';
            $results = $wpdb->get_results("SELECT * FROM $table_name WHERE `state`=0");
            $parts = $results;
            
            $requiredComponents = [];
            foreach($parts as $partMFG){
				$table_name = $wpdb->prefix.'warehouse_parts';
				$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$partMFG->id_part);
				$part = $results[0];
                addMultiPartManufacture($partMFG->id_part, $partMFG->quantity, 0, array('name'=>$part->partname, 'qty'=>$partMFG->quantity));
            }

            $type  = array_column($requiredComponents, 'type');
            $partname = array_map('strtolower', array_column($requiredComponents, 'partname'));
            array_multisort($type, SORT_ASC, $partname, SORT_ASC, $requiredComponents);	

            $x = array( 'SKU', 
                        'Partname', 
                        'Description',    
                        'Manufacturer',  
                        'Type', 
                        'Req. quantity',
                        'Order quantity', 
                        'Quantity Per Package', 
                        'Package Type',   
                        'Price', 
                        'Total price',
                        'Currency', 
                        'Current Supplier',    
                        'Lead Time',    
                        'MOQ'              
            );
            array_push($array, $x);

            foreach ($requiredComponents as $key => $value) {
                if($value['quantity'] <= 0)
                    continue;

                    //echo $key . ' -> ' . $value['quantity'] . '<br>';
                    $table_name = $wpdb->prefix.'warehouse_parts';
	                $results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$value['id']);
                    $part = $results[0];

                    $reqQTY = $value['quantity'];
                    if($part->quantityPerBox > 1){
						$orgQTY = $value['quantity'];
						$value['quantity'] = roundUpStep($value['quantity'],$part->quantityPerBox);
					}
                    $partPrice = $part->price * $value['quantity'];

                $x = array( $part->sku,
                            $part->partname, 
                            $part->description, 
                            $part->manufacturer,  
                            $part->type, 
                            $reqQTY, 
                            $value['quantity'], 
                            $part->quantityPerBox, 
                            $part->boxPackageType,   
                            $part->price, 
                            $partPrice,
                            $part->currency, 
                            $part->currentSupplier,    
                            $part->leadTime,    
                            $part->moq              
                );
                array_push($array, $x);
            }

            foreach($array as $line){
                fputcsv($fh, $line, ','); 
            }
        
            fclose( $fh );
            ob_end_flush();
            break;

        case 'exportMultiparts':
            $table_name = $wpdb->prefix.'warehouse_parts';
            $results = $wpdb->get_results("SELECT * FROM $table_name ORDER BY `type` ASC, `partname` ASC");
            $parts = $results;
            $x = array( 'partname', 
                        'sku'            
            );
            array_push($array, $x);

            foreach($parts as $part){
                $price = $part->price;
                $currency = $part->currency;
                if($part->multipart){
                    $price = calculateTotalPartPrice($part->id);
                    $currency = getSettingsValue("CURRENCY_HOME");
                }
                $x = array( $part->partname, 
                            $part->description,    
                            $part->details,
                            $part->manufacturer,  
                            $part->sku, 
                            $part->type, 
                            $part->quantity, 
                            $part->quantityPerBox, 
                            $part->boxPackageType,   
                            $price, 
                            $currency, 
                            $part->position,    
                            $part->currentSupplier,    
                            $part->leadTime,    
                            $part->moq              
                );
                array_push($array, $x);
            }
            break;
    } 

    die();
}

if(isset($_POST['exportCSV'])){
    add_action( 'admin_init', 'exportCSV' );
}

?>