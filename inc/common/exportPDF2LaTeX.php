<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once  WAREHOUSE_DIR_SERVER."inc/common/functions.php";

function exportPartToPDF_LaTeX($partID){
	global $wpdb;

    $parts = addMultiPart($partID, 1);
    // echo print_r($parts);

    $partsTable = "";

    $totalPrice = 0;
    $homeCurrency = getSettingsValue("CURRENCY_HOME");
	
    foreach ($parts['childs'] as $part){
        $linePrice = calculateTotalPartPrice($part['id']);
        $lineTotalPrice = $linePrice * $part['qty'];
        $partsTable .= "\\multirow{2}{*}{".$part['type']."} & \\multirow{2}{*}{".$part['sku']."} & \\textbf{".$part['partname']."} & \\multirow{2}{*}{".$part['qty']."} & \\multirow{2}{*}{".round($linePrice,3)."} & \\multirow{2}{*}{".round($lineTotalPrice,3)."} & \\multirow{2}{*}{".$homeCurrency. "} \\\\ \n";
        $partsTable .= " & & \\multicolumn{5}{l}{\\textit{\\small ".$part['description']."}}  \\\\ \n \\hline \n";
        $totalPrice += $lineTotalPrice;
    }

    $texFile = fopen(WAREHOUSE_DIR_SERVER."inc/tex/partExport.tex", "r") or die("Unable to open file!");
    $sourceTex =  fread($texFile,filesize(WAREHOUSE_DIR_SERVER."inc/tex/partExport.tex"));
    fclose($texFile);

    $partsTable = str_replace("_", "\_", $partsTable );
    $partsTable = str_replace("%", "\%", $partsTable );

	$sourceTex = str_replace("++PART-SHORT-DESCRIPTION++", str_replace("_", "\_",$parts['description']), $sourceTex );
    $sourceTex = str_replace("++PART-BARCODE++", str_replace("_", "\_",$parts['barcode']), $sourceTex );
    $sourceTex = str_replace("++PART-NAME++", str_replace("_", "\_",$parts['partname']), $sourceTex );
    $sourceTex = str_replace("++PART-SKU++", str_replace("_", "\_",$parts['sku']), $sourceTex );
    $sourceTex = str_replace("++PART-MANUFACTURER++", str_replace("_", "\_",$parts['manufacturer']), $sourceTex );
    $sourceTex = str_replace("++PART-TYPE++", str_replace("_", "\_",$parts['type']), $sourceTex );
    $sourceTex = str_replace("++PART-PRICE++", round($totalPrice,3), $sourceTex );
    $sourceTex = str_replace("++PART-CURRENCY++", $homeCurrency, $sourceTex );
    $sourceTex = str_replace("++PART-SHIPPING-TIME++", $parts['leadTime'], $sourceTex );
    $sourceTex = str_replace("++PART-SUPPLIER++", str_replace("_", "\_",$parts['currentSupplier']), $sourceTex );
    $sourceTex = str_replace("++PART-MOQ++", $parts['moq'], $sourceTex );
    $sourceTex = str_replace("++PART-PACKAGE-QUANTITY++", $parts['quantityPerBox'], $sourceTex );
    
    if($parts['datasheet'] == "")
        $sourceTex = str_replace("++PART-DATASHEET++", 'not available', $sourceTex );
    else
        $sourceTex = str_replace("++PART-DATASHEET++", "\href{".$parts['datasheet']."}{Available}\\footnote{\\url{".$parts['datasheet']."}}", $sourceTex );

    $sourceTex = str_replace("++TABLE-COMPONENTS++", $partsTable, $sourceTex );
    
    $sourceTex = str_replace("++DATE-CREATED++", date("d.m.Y"), $sourceTex );

    if($parts['details'] != ""){
        $textDetails = "Note:\n\n".$parts['details'];
    }
    else{
        $textDetails = "";
    }

    $sourceTex = str_replace("++PART-NOTE++", str_replace("_", "\_",$textDetails), $sourceTex );

    $tempDirName = generateRandomString();
    $tempDir = WP_TEMP_DIR.$tempDirName."/";

    //echo $tempDir."</br>";

    if(!is_dir($tempDir)){
        //Directory does not exist, so lets create it. 
        mkdir($tempDir, 0755, true);
    }

    $texFile = fopen($tempDir."partExport.tex", "w") ;
    fwrite($texFile, $sourceTex);
    fclose($texFile);

    // echo "Output value: ".nl2br($sourceTex); 


    $command = getSettingsValue("LATEX_COMMAND");

    $command = str_replace("**LOCAL_DIR**", $tempDir, $command );
    $command = str_replace("**FILE**", "partExport.tex", $command );
 
    // echo "command: ".$command."</br>";

    $output = 0;
    $return_var = 0;
    
    //$cmd_out = shell_exec($command);   


    // echo "Output value: ".nl2br($cmd_out); 

    exec( $command, $output, $return_var );
    // echo "exec: ";
    // print_r($output);
    // echo ", ".$return_var."</br>";
    exec( $command, $output, $return_var );
    //echo "exec: ";
    // print_r($output);
    //echo ", ".$return_var."</br>";


	if(!file_exists($tempDir."partExport.pdf")){
		return "Unfortunately, the export wasn't performed. Please contact your administrator to setup LaTeX";
		deleteDir($tempDir);
	}
	else{
		
			
		$partsListDir = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/warehouse/partsList/";
		if(!is_dir($partsListDir)){
			//Directory does not exist, so lets create it. 
			mkdir($partsListDir, 0755, true);
		}

		$newFileName = $partsListDir.strtolower($parts['partname']).".pdf";
		if(file_exists($newFileName)){
			unlink($newFileName);
		}
		rename($tempDir."partExport.pdf", $newFileName);

		// 
		deleteDir($tempDir);
		return "<a href='/wp-content/uploads/warehouse/partsList/".strtolower($parts['partname']).".pdf'"."> Download Here</a>";
	}
	
}


?>