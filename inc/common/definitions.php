<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// DEFINES for Order parts
define("IN_CART",    0);
define("OPEN_ORDER", 1);
define("QUOTATION",  2);
define("ON_ORDER",   3);
define("DELIVERED",  4);

// DEFINES for Manufacture parts
define("MANUFACTURE_ON_PERPARATION",    0);
define("MANUFACTURE_IN_PRODUCTION",     1);
define("MANUFACUTRE_DONE",              2);

?>