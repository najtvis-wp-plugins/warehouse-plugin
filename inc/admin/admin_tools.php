<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// login form
function conf_sc_admin_lists($atts) {
	global $wpdb;
	if(session_id() == '')
		session_start(); 

	if (isset($_SESSION['userID']) && isset($_SESSION['userEmail'])) {		
		// read user information
		$table_name = $wpdb->prefix.'conference_users';
		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE id='".$_SESSION["userID"]."'");
		$user = $results[0];
	} 
	else {
		conference_redirect(get_permalink( get_page_by_path( 'conference/abstracts-to-edit' )));
		exit;
	}
	
	ob_start();
?>
	
	<?php if($user->privileges == "admin" || $user->privileges ==  "super" || $user->privileges ==  "accountant" ) { 	
		?>
		
		<div style="overflow-x:auto;white-space: nowrap;">
			<?php 
			if(isset($_GET['attendance'])){
				?>
				<div class="conf_block">
					<h1 class="conference">List of Participants Attendence</h1>
				</div>
				<?php
				echo getParticipantsAttendance(); 
				echo do_shortcode("[conf_no_countries type='plain' year='last']");
			}
			else if(isset($_GET['addresses'])){
				?>
				<div class="conf_block">
					<h1 class="conference">List of Participants Addresses</h1>
				</div>
				<?php
				echo getParticipantsAddresses(); 
			}
			else if(isset($_GET['invoices'])){
				?>
				<div class="conf_block">
					<h1 class="conference">Seznam chybějících faktur pro účastníky <?php echo do_shortcode("[conf_name]")." ".do_shortcode("[conf_year]"); ?></h1>
				</div>
				<?php
				echo getListOfInvoices(); 
			}	
			else if(isset($_GET['sendRequest'])){
				sendRequestForAccountant();
			}
			else if(isset($_GET['sendToParticipants'])){
				sendInvoicesToParticipants();
			}
			else {
				echo "No tables";
			}
			?>
		</div>
	<?php }
	else{ 
	?>
		Your are not allowed to see data.
	<?php } 
	
	
	?>	

	<?php
	return ob_get_clean();
}
add_shortcode('conf_admin_tools', 'conf_sc_admin_lists'); 
?>
