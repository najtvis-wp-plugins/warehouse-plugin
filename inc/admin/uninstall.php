<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}

register_uninstall_hook( __FILE__, 'warehouse_uninstall' );
function warehouse_uninstall() {
    $role = get_role( 'administrator' );
    if ( ! empty( $role ) ) {

    }
}

?>