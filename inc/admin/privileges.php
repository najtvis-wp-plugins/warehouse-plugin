<?php

/**
 * Template Name: List of Parts
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function is_wp_login() {
    echo $GLOBALS['pagenow'] ;
    $_SESSION['warehouse_message'] = "TEST";
    if ( $GLOBALS['pagenow'] === 'wp-login.php' ) {
        echo "yes";
    }
    return !strncmp($_SERVER['REQUEST_URI'], '/wp-login.php', strlen('/wp-login.php'));

}

function user_has_role($user_id, $role_name)
{
    $user_meta = get_userdata($user_id);
    $user_roles = $user_meta->roles;
    return in_array($role_name, $user_roles);
}

// Add the user privileges to the user/profile in WP users.
add_action( 'show_user_profile', 'warehouse_show_user_metadata' );
add_action( 'edit_user_profile', 'warehouse_show_user_metadata' );
function warehouse_show_user_metadata( $user ) {
    ?> <h2>Warehouse Info</h2> 
    <?php
    $current_user = wp_get_current_user();
    $privileges = get_user_meta( $current_user->ID, 'warehouse_privileges', true );
    if($privileges == 'super' || user_has_role($current_user->ID, "administrator")){
        $edited_privileges = get_user_meta( $user->ID, 'warehouse_privileges', true );
    ?>
    
    <table class="form-table">
        <tr>
            <th>Set privileges to:</th>
            <td>
                <select name="warehouse_privileges" id="warehouse_privileges">
                    <option value=" "       <?php selected( '', $edited_privileges, 'selected' ); ?>       ></option>
                    <option value="viewer"  <?php selected( 'viewer', $edited_privileges, 'selected' ); ?> >Viewer</option>
                    <option value="editor"  <?php selected( 'editor', $edited_privileges, 'selected' ); ?> >Editor</option>
                    <option value="warehouseman"  <?php selected( 'warehouseman', $edited_privileges, 'selected' ); ?> >Warehouseman</option>
                    <option value="super"   <?php selected( 'super', $edited_privileges, 'selected' ); ?>  >Super User</option>
                </select>
                <br />
                <span class="description">Associate the user with its privileges</span>
            </td>
        </tr>
    </table>
    <?php 
    }

    
    $privileges = get_user_meta( $user->ID, 'warehouse_privileges', true );
    ?> 
    <table class="form-table">
        <tr>
            <th>Privilege level:</th>
            <td><?php  
                switch($privileges){
                    case 'super':
                        echo "Super User";
                        break;
                    case 'viewer':
                        echo "Viewer";
                        break;
                    case 'editor':
                        echo "Editor";
                        break;
                    case 'warehouseman':
                        echo "Warehouseman";
                        break;
                    default:
                        echo "None";
                }
                ?>
            </td>
        </tr>
        <tr>
            <th>Can View:</th>
            <td><?php  
                $privilege = get_user_meta( $user->ID, 'warehouse_can_view', true );
                if($privilege == "1")
                    echo '<span class="dashicons dashicons-yes" style="color: green;"></span>';
                else
                    echo '<span class="dashicons dashicons-no" style="color: red;"></span>';
                ?>
            </td>
        </tr>
        <tr>
            <th>Can Edit Parts:</th>
            <td><?php  
                $privilege = get_user_meta( $user->ID, 'warehouse_can_edit', true );
                if($privilege == "1")
                    echo '<span class="dashicons dashicons-yes" style="color: green;"></span>';
                else
                    echo '<span class="dashicons dashicons-no" style="color: red;"></span>';
                ?>
            </td>
        </tr>
        <tr>
            <th>Can Import/Export Goods:</th>
            <td><?php  
                $privilege = get_user_meta( $user->ID, 'warehouse_can_import_export', true );
                if($privilege == "1")
                    echo '<span class="dashicons dashicons-yes" style="color: green;"></span>';
                else
                    echo '<span class="dashicons dashicons-no" style="color: red;"></span>';
                ?>
            </td>
        </tr>
        <tr>
            <th>Is Admin:</th>
            <td><?php  
                $privilege = get_user_meta( $user->ID, 'warehouse_is_admin', true );
                if($privilege == "1")
                    echo '<span class="dashicons dashicons-yes" style="color: green;"></span>';
                else
                    echo '<span class="dashicons dashicons-no" style="color: red;"></span>';
                ?>
            </td>
        </tr>
    </table>
    <?php
}

// Add the update function to the user update hooks.
add_action( 'personal_options_update', 'warehouse_update_user_metadata' );
add_action( 'edit_user_profile_update', 'warehouse_update_user_metadata' );

function warehouse_update_user_metadata( $user_id ) {
    $current_user = wp_get_current_user();
    $privileges = get_user_meta( $current_user->ID, 'warehouse_privileges', true );
    if($privileges != 'super' && !user_has_role($current_user->ID, "administrator"))
        return;

    $old_privileges = get_user_meta( $user_id, 'warehouse_privileges', true );
    $new_privileges = $_POST['warehouse_privileges'];
    
    // Update the user's privileges for warehouse    
    if ( $new_privileges !== $old_privileges ) {
        update_user_meta( $user_id, 'warehouse_privileges', $new_privileges );

        switch($new_privileges){
            case 'viewer':
                update_user_meta( $user_id, 'warehouse_can_view', "1");
                update_user_meta( $user_id, 'warehouse_can_edit', "0");
                update_user_meta( $user_id, 'warehouse_can_import_export', "0");
                update_user_meta( $user_id, 'warehouse_is_admin', "0");
                break;
            case 'editor':
                update_user_meta( $user_id, 'warehouse_can_view', "1");
                update_user_meta( $user_id, 'warehouse_can_edit', "1");
                update_user_meta( $user_id, 'warehouse_can_import_export', "0");
                update_user_meta( $user_id, 'warehouse_is_admin', "0");
                break;
            case 'warehouseman':
                update_user_meta( $user_id, 'warehouse_can_view', "1");
                update_user_meta( $user_id, 'warehouse_can_edit', "0");
                update_user_meta( $user_id, 'warehouse_can_import_export', "1");
                update_user_meta( $user_id, 'warehouse_is_admin', "0");
                break;
            case 'super':
                echo "edited super";
                update_user_meta( $user_id, 'warehouse_can_view', "1");
                update_user_meta( $user_id, 'warehouse_can_edit', "1");
                update_user_meta( $user_id, 'warehouse_can_import_export', "1");
                update_user_meta( $user_id, 'warehouse_is_admin', "1");
                break;
        }
    }
}

function check_pages_privileges(){
    if(!is_user_logged_in()){
        $_SESSION['warehouse_message'] = "<center><font color='red'>We are sorry, you have no sufficient privileges to view this page. Please, log in.</font></center>";
    
        wp_redirect(wp_login_url());
        exit;
        return;
    }

    $current_user = wp_get_current_user();
    // decide if the user can view the page
    $can_view = get_user_meta( $current_user->ID, 'warehouse_can_view', true);
    $can_edit = get_user_meta( $current_user->ID, 'warehouse_can_edit', true);
    $can_import_export = get_user_meta( $current_user->ID, 'warehouse_can_import_export', true);
    $is_admin = get_user_meta( $current_user->ID, 'warehouse_is_admin', true);

    if( is_page( 'warehouse' ) 
        && 
        $can_view == '0') {	
        $_SESSION['warehouse_message'] = "<center><font color='red'>We are sorry, you have no sufficient privileges to view this page. Please log in to different account.</font></center>";
        wp_redirect(wp_login_url());
        exit;
	}

    if((is_page( 'edit_component' ) || 
        is_page( 'new_component' )) 
        && 
        $can_edit == '0') {	
        $_SESSION['warehouse_message'] = "<center><font color='red'>We are sorry, you have no sufficient privileges to view this page</font></center>";
        wp_redirect(home_url());
        exit;
	}

    if((is_page( 'import_goods_to_warehouse' ) || 
        is_page( 'export_goods_from_warehouse' )) 
        && 
        $can_import_export == '0') {	
        $_SESSION['warehouse_message'] = "<center><font color='red'>We are sorry, you have no sufficient privileges to view this page</font></center>";
        wp_redirect(home_url());
        exit;
	}

    if(is_page( 'list_of_multiparts' ) && !isset($_GET['part_id'])){
        $_SESSION['warehouse_message'] = "<center><font color='red'>We are sorry, you didn't select any valid part, please select one from the following table</font></center>";
        wp_redirect(home_url());
        exit;
    }
}

add_action( 'template_redirect', 'check_pages_privileges' );

function warehouse_login_message( $message ) {
    if(isset($_SESSION['warehouse_message'])){
        $msg = $_SESSION['warehouse_message'];
		unset($_SESSION['warehouse_message']);
		return $msg;
	} 
}

add_filter( 'login_message', 'warehouse_login_message' );

function _custom_nav_menu_item( $title, $url, $order, $parent = 0 ){
    $item = new stdClass();
    $item->ID = 1000000 + $order + $parent;
    $item->db_id = $item->ID;
    $item->title = $title;
    $item->url = $url;
    $item->menu_order = $order;
    $item->menu_item_parent = $parent;
    $item->type = '';
    $item->type_label = '';
    $item->object = '';
    $item->object_id = '';
    $item->classes = array();
    $item->target = '';
    $item->attr_title = '';
    $item->description = '';
    $item->xfn = '';
    $item->status = '';
    return $item;
}

add_filter( 'wp_get_nav_menu_items', 'warehouseNavigationMenu', 20, 2 );

function warehouseNavigationMenu( $items, $menu ) {
    $current_user = wp_get_current_user();
    // decide if the user can view the page
    $can_view = get_user_meta( $current_user->ID, 'warehouse_can_view', true);
    $can_edit = get_user_meta( $current_user->ID, 'warehouse_can_edit', true);
    $can_import_export = get_user_meta( $current_user->ID, 'warehouse_can_import_export', true);
    $is_admin = get_user_meta( $current_user->ID, 'warehouse_is_admin', true);
    
    if($can_view == '1'){
        $items[] = _custom_nav_menu_item( 'Warehouse', get_permalink( get_page_by_path( 'warehouse' )), 100 );
    }

    if($can_edit == '1'){
        $items[] = _custom_nav_menu_item( 'New', get_permalink( get_page_by_path( 'warehouse/new_component' )), 200 );
        //$items[] = _custom_nav_menu_item( 'Edit', get_permalink( get_page_by_path( 'warehouse/edit_component' )), 201 );
    }

    if($can_import_export == '1'){
        $items[] = _custom_nav_menu_item( 'Import', get_permalink( get_page_by_path( 'warehouse/import_goods_to_warehouse' )), 300 );
        $items[] = _custom_nav_menu_item( 'Export', get_permalink( get_page_by_path( 'warehouse/export_goods_from_warehouse' )), 301 );
        $items[] = _custom_nav_menu_item( 'Order', get_permalink( get_page_by_path( 'warehouse/order_components' )), 302 );
        $items[] = _custom_nav_menu_item( 'Manufacture', get_permalink( get_page_by_path( 'warehouse/manufacture' )), 303 );
        $items[] = _custom_nav_menu_item( 'Cart', get_permalink( get_page_by_path( 'warehouse/cart' )), 304 );
    }    

  return $items;
}


?>