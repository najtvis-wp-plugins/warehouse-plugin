<?php

// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}

include_once  WAREHOUSE_DIR_SERVER."inc/common/functions.php";
include_once  WAREHOUSE_DIR_SERVER."inc/common/exportCSV.php";

add_action('admin_menu', 'admin_plugin_menu');
function admin_plugin_menu() { 

add_menu_page( 
    'Warehouse Admin Tools', 
    'Warehouse plugin', 
    'edit_posts', 
    'warehouse_global', 
    'warehouse_admin_global', 
    'dashicons-admin-users' 
   );
//add_submenu_page( 'conference_global', 'Conference - Emails', 'Emails', 'manage_options', 'conference_admin_emails', 'conference_admin_emails' );
//add_submenu_page( 'conference_global', 'Conference - Sessions', 'Sessions', 'manage_options', 'conference_admin_sessions', 'conference_admin_sessions' );
//add_submenu_page( 'conference_global', 'Conference - Fees', 'Fees', 'manage_options', 'conference_admin_fees', 'conference_admin_fees' );
}

function global_tabs( $current = 'warehouse' ) {
    $tabs = array(
		//'privileges'   => __( 'Users Privileges', 'plugin-textdomain' ), 
        'partType'          => __( 'Part type SKU definition', 'plugin-textdomain' ), 
        'export'            => __( 'Export', 'plugin-textdomain' ), 
        'emailSettings'     => __( 'Send email settings', 'plugin-textdomain' ),		
		'deploy'            => __( 'Re/Deploy plugin', 'plugin-textdomain' ),
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=warehouse_global&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

function warehouse_admin_global(){
	global $wpdb;
	
	?>
    <div class="wrap">
		<h1>Settings for Warehouse plugin</h1>
		<?php
		$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'warehouse';
		global_tabs( $tab );
		
		switch ( $tab ){
			case 'partType' :
                if(isset($_POST['savePartTypes']) || isset($_POST['numberAllMissingSKUs']) || isset($_POST['renumberAllSKUs'])){
                    $allTypes = [];
                    foreach ($_POST as $key => $value) {
                        if(strpos($key, "partType") !== false){
                            $id = str_split($key, strlen("partType"))[1];
                            $skuPrefixID = "sku".$id;
                            $skuPrefix = $_POST[$skuPrefixID];
                            $descriptionID = "desc".$id;
                            $description = $_POST[$descriptionID];
                            
                            $selSKU = "selSKU".$id;
                            if(isset($_POST[$selSKU]))
                                $_POST[$value]=1;
                            array_push($allTypes,  array(   'type'          => $value,
                                                            'skuPrefix'     => $skuPrefix,
                                                            'description'   => $description
                            ));

                            if(isset($_POST['numberAllMissingSKUs'])){
                                $table_name = $wpdb->prefix.'warehouse_parts';
                                $parts = $wpdb->get_results("SELECT * FROM $table_name WHERE `sku`='' AND `type`='".$value."'");
                                // $lastPrefixValue = strval(getSettingsValue("LAST_SKU_PREFIX_VAL_".$skuPrefix));
                                // $prefixLen = strval(getSettingsValue("SKU_PREFIX_LEN"));
	                            
                                foreach($parts as $part){
                                    /*do{
                                        $skuNumber = str_pad($lastPrefixValue++, $prefixLen, '0', STR_PAD_LEFT);
                                        $sku = $skuPrefix.$skuNumber;
                                        $res = $wpdb->get_results("SELECT * FROM $table_name WHERE `sku`='".$sku."' ORDER BY `type` ASC, `partname` ASC");
                                    }
                                    while(!empty($res));
                                    $data = array('sku' => $sku);
                                    $where = array('id' => $part->id);
                                    $wpdb->update($table_name, $data, $where);*/
                                    addSKUNumber($part->id, $skuPrefix);
                                }
                                // updateSettingsValue("LAST_SKU_PREFIX_VAL_".$skuPrefix, $lastPrefixValue);
                                
                            }
                            else if(isset($_POST['renumberAllSKUs'])){
                                if(isset($_POST[$selSKU])){
                                    $table_name = $wpdb->prefix.'warehouse_parts';
                                    $parts = $wpdb->get_results("SELECT * FROM $table_name WHERE `type`='".$value."' ORDER BY `type` ASC, `partname` ASC");
                                    $lastPrefixValue = 0;
                                    $prefixLen = strval(getSettingsValue("SKU_PREFIX_LEN"));
                                    $i = 0;
                                    
                                    foreach($parts as $part){
                                        $skuNumber = str_pad($lastPrefixValue++, $prefixLen, '0', STR_PAD_LEFT);
                                        $sku = $skuPrefix.$skuNumber;
                                        $data = array('sku' => $sku);
                                        $where = array('id' => $part->id);
                                        $wpdb->update($table_name, $data, $where);
                                    }
                                    updateSettingsValue("LAST_SKU_PREFIX_VAL_".$skuPrefix, $lastPrefixValue);
                                }
                            }
                        }
                    }

                    if(isset($_POST['savePartTypes'])){
                        $type  = array_column($allTypes, 'type');
                        array_multisort($type, SORT_ASC, $allTypes);
                        updateSettingsValue("PART_TYPES_DEF", json_encode($allTypes));
                        updateSettingsValue("SKU_PREFIX_LEN", $_POST['prefixLen']);
                    }
                }  

                $storedPartTypes = json_decode(getSettingsValue("PART_TYPES_DEF"), true);
                if($storedPartTypes == "")
                    $storedPartTypes = array();              
                
                ?>
                <script>
                    var lastID = <?php echo count($storedPartTypes); ?>;
                    function deleteRow(row) {
                        var i = row.parentNode.parentNode.rowIndex;
                        document.getElementById('partTypeTable').deleteRow(i);
                    }

                    function addRow() {
                        var tableID = "partTypeTable";
                        var table = document.getElementById(tableID);
                        var rowCount = table.rows.length;

                        var row = table.insertRow(rowCount);
                        var newcell = row.insertCell(0);
                        
                        newcell.innerHTML = "<input class='skuCheckboxes' id='selSKU"+lastID+"' name='selSKU"+lastID+"' type='checkbox'></input>";
                        newcell = row.insertCell(1);

                        newcell.innerHTML = "<input id='partType"+lastID+"' name='partType"+lastID+"' type='text' style='width: 100%;' required></input>";
                        newcell = row.insertCell(2);

                        newcell.innerHTML = "<input id='sku"+lastID+"' name='sku"+lastID+"' type='text' style='width: 100%;' required></input>";
                        newcell = row.insertCell(3);

                        newcell.innerHTML = "<input id='desc"+lastID+"' name='desc"+lastID+"' type='text' style='width: 100%;' required></input>";
                        newcell = row.insertCell(4);

                        newcell.innerHTML = "<a class='dashicons dashicons-trash' onclick='deleteRow(this)'/>";
                        ++lastID;
                    }

                    function do_this(){
                        var checkboxes = document.getElementsByClassName('skuCheckboxes');
                        var button = document.getElementById('select-all');

                        if(button.checked == ''){
                            for (var i in checkboxes){
                                checkboxes[i].checked = '';
                            }
                        }else{
                            for (var i in checkboxes){
                                checkboxes[i].checked = 'TRUE';
                            }
                        }
                    }
                </script>

                <style>
                    .form-table th,
                    .form-table td {
                        padding: 5px 10px 5px 10px;
                    }
                </style>

                <form action="" method="post">
                    <h3>General setup</h3>
                    <table>
                        <tr>
                            <th>Length of SKU numbers:</th>
                            <td><input type="number" id="prefixLen" min='1' name="prefixLen" value="<?php echo getSettingsValue("SKU_PREFIX_LEN"); ?>" /></td>
                        </tr>
                    </table> 

                    <h3>List of part types</h3>
                    <table class="form-table" id="partTypeTable">
                        <tr>
                            <th class="stockCSS" style="text-align: center; "><input type='checkbox' name='select-all' id='select-all' onClick='do_this(this)'/></th>
                            <th class="stockCSS" style="text-align: center; width:10%;">Part type</th>
                            <th class="stockCSS" style="text-align: center; width:10%;">SKU Prefix</th>
                            <th class="stockCSS" style="text-align: center; width:75%;">Description</th>
                            <th class="stockCSS" style="text-align: center; width:5%;"></th>
                        </tr>
                        <?php
                            $i=0;
                            foreach($storedPartTypes as $type){
                                echo "<tr>";
                                if(isset($_POST[$type['type']]))
                                    echo "  <td class='stockCSS'><input class='skuCheckboxes' id='selSKU".$i."' name='selSKU".$i."' type='checkbox' checked></input></td>";
                                else
                                    echo "  <td class='stockCSS'><input class='skuCheckboxes' id='selSKU".$i."' name='selSKU".$i."' type='checkbox'></input></td>";
                                echo "  <td class='stockCSS'><input id='partType".$i."' name='partType".$i."' type='text' style='width: 100%;' value='".$type['type']."' required></input></td>";
                                echo "  <td class='stockCSS'><input id='sku".$i."' name='sku".$i."' type='text' style='width: 100%;' value='".$type['skuPrefix']."' required></input></td>";
                                echo "  <td class='stockCSS'><input id='desc".$i."' name='desc".$i."' type='text' style='width: 100%;' value='".$type['description']."' required></input></td>";
                                echo "  <td class='stockCSS'><a class='dashicons dashicons-trash' onclick='deleteRow(this)'/></td>";
                                echo "</tr>";
                                ++$i;
                            }
                        ?>
                    </table>
                    <input type="button" id="addPartTypeRow" onClick="addRow()" value="Add row" />
                    <div class="submit">
                        <button  type="submit" class="button-primary" name="savePartTypes" id="savePartTypes" >Save changes</button>

                        <button  type="submit" class="button-primary" name="numberAllMissingSKUs" id="numberAllMissingSKUs" >Number all missing SKUs</button>

                       <button  type="submit" class="button-primary" name="renumberAllSKUs" id="renumberAllSKUs" >Re-number all selected SKUs</button>
                    </div>
                </form>
                
                <?php
                break;
            case 'export' :
                ?>
                
				<form action="" method="post">
                    <h3>Export Parts</h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    Filename:
                                </th>
                                <td>
                                    <input id="CSVfilename" type='text'  class="regular-text" name="CSVfilename" value="parts.csv"/>
                                </td>
                                <td>
                                    <button  type="submit" class="button-primary" value="exportParts" name="exportCSV" />Export parts in csv</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
                
                <form action="" method="post">
                    <h3>Export Manufacture list</h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    Filename:
                                </th>
                                <td>
                                    <input id="CSVfilename" type='text'  class="regular-text" name="CSVfilename" value="manufacture_list.csv"/>
                                </td>
                                <td>
                                    <button  type="submit" class="button-primary" value="exportManufacture" name="exportCSV" />Export manufacture list in csv</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>

                <form action="" method="post">
                    <h3>Export Multiparts pack</h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    Filename:
                                </th>
                                <td>
                                    <input id="CSVfilename" type='text'  class="regular-text" name="CSVfilename" value="multiparts.zip"/>
                                </td>
                                <td>
                                    <button  type="submit" class="button-primary" value="exportMultiparts" name="exportCSV" />Export all multiparts in csv pack</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>

                <?php
                break;

                case 'emailSettings' :
                    if(isset($_POST['saveWarehouseEmailSettings'])){
                         updateSettingsValue('SMTP_HOST', $_POST['warehouseSMTP_HOST']);
                         updateSettingsValue('SMTP_PORT', $_POST['warehouseSMTP_PORT']);
                         updateSettingsValue('SMTP_SECURE', $_POST['warehouseSMTP_SECURE']);
                         updateSettingsValue('SMTP_USERNAME', $_POST['warehouseSMTP_USERNAME']);
                         updateSettingsValue('SMTP_PASSWORD', $_POST['warehouseSMTP_PASSWORD']);
                         updateSettingsValue('SMTP_FROM', $_POST['warehouseSMTP_FROM']);
                         updateSettingsValue('SMTP_FROMNAME', $_POST['warehouseSMTP_FROMNAME']);
                    }
                    $warehouseSMTP_HOST = getSettingsValue('SMTP_HOST');
                    $warehouseSMTP_PORT = getSettingsValue('SMTP_PORT');
                    $warehouseSMTP_SECURE = getSettingsValue('SMTP_SECURE');
                    $warehouseSMTP_USERNAME = getSettingsValue('SMTP_USERNAME');
                    $warehouseSMTP_PASSWORD = getSettingsValue('SMTP_PASSWORD');
                    $warehouseSMTP_FROM = getSettingsValue('SMTP_FROM');
                    $warehouseSMTP_FROMNAME = getSettingsValue('SMTP_FROMNAME');
                    
                    ?>
                    <form action="" method="post">
                        <h3>Email settings</h3>
                        <table class="form-table">
                            <tbody>
                                <tr>
                                    <th>
                                        SMTP host
                                    </th>
                                    <td>
                                        <input id="warehouseSMTP_HOST" type='text'  class="regular-text" name="warehouseSMTP_HOST" value="<?php echo $warehouseSMTP_HOST; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        SMTP port
                                    </th>
                                    <td>
                                        <input id="warehouseSMTP_PORT" type='text' class="regular-text" name="warehouseSMTP_PORT" value="<?php echo $warehouseSMTP_PORT; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        SMTP secure
                                    </th>
                                    <td>
                                        <input id="warehouseSMTP_SECURE" type='text'  class="regular-text" name="warehouseSMTP_SECURE" value="<?php echo $warehouseSMTP_SECURE; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        SMTP username
                                    </th>
                                    <td>
                                        <input id="warehouseSMTP_USERNAME" type='text'  class="regular-text" name="warehouseSMTP_USERNAME" value="<?php echo $warehouseSMTP_USERNAME; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        SMTP password
                                    </th>
                                    <td>
                                        <input id="warehouseSMTP_PASSWORD" type='password'  class="regular-text" name="warehouseSMTP_PASSWORD" value="<?php echo $warehouseSMTP_PASSWORD; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        SMTP from
                                    </th>
                                    <td>
                                        <input id="warehouseSMTP_FROM" type='text' class="regular-text" name="warehouseSMTP_FROM" value="<?php echo $warehouseSMTP_FROM; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        SMTP from name
                                    </th>
                                    <td>
                                        <input id="warehouseSMTP_FROMNAME" type='text' class="regular-text" name="warehouseSMTP_FROMNAME" value="<?php echo $warehouseSMTP_FROMNAME; ?>"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <div class="submit">
                            <button  type="submit" class="button-primary" name="saveWarehouseEmailSettings" />Save changes</button>
                        </div>
                    </form>
        <?php
        break;
        case 'deploy' :
            if(isset($_POST['submit_create'])){
                install_warehouse_plugin_tables();
                install_warehouse_plugin_pages();
                install_warehouse_plugin_settings();
            }
            ?>
            <p>
                If warehouse pages, settings, etc. exist, they will NOT be replaced.
            </p>
            <form action="" method="post">
                <div class="one_column">
                    <button name="submit_create" class="button-primary" type="submit" id="submit_create">Create warehouse</button>
                </div>
            </form>
            <?php
        break;
        }
    
}

?>