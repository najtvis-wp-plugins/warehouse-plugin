<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}

include_once  WAREHOUSE_DIR_SERVER."inc/common/functions.php";

function install_warehouse_plugin_settings() {
    global $wpdb;

	// email setup
	createSettingsValue('SMTP_HOST', "mail.example.com");
	createSettingsValue('SMTP_PORT', "465");
	createSettingsValue('SMTP_SECURE', "ssl");
	createSettingsValue('SMTP_USERNAME', "info@example.com");
	createSettingsValue('SMTP_PASSWORD');
	createSettingsValue('SMTP_FROM', "info@example.com");
	createSettingsValue('SMTP_FROMNAME', "John Smith");

	// receipts numbers
	createSettingsValue('ORDER_NUMBER', '0');
	createSettingsValue('EXPORT_RECEIPT_NUMBER', '0');
	createSettingsValue('IMPORT_RECEIPT_NUMBER', '0');

	// currency
	createSettingsValue('CURRENCY_HOME', '0');

	// general settings
	createSettingsValue('LATEX_COMMAND', 'docker run -i --rm --name latex_docker -v **LOCAL_DIR**:/usr/src/app -w /usr/src/app listx/texlive:2020 xelatex **FILE**');
	
}

function install_warehouse_plugin_tables() {
    global $wpdb;
	
	echo "<b>database installation started</b><br>";
	$table_name = $wpdb->prefix.'warehouse_parts';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  sku text NULL,
			  value text NULL,
			  partname text NULL,
			  replacement text NULL,
			  barcode text NULL,
			  manufacturer text NULL,
			  package text NULL,
			  details text NULL,
			  description text NULL,
			  position text NULL,
			  type text NULL,
			  price float NULL,
			  currency text NULL,
			  quantity double,
			  quantityPerBox int,
			  boxPackageType text,
			  moq int,
			  minQuantity float,
			  posColour tinytext,
			  currentSupplier text,
			  leadTime int NULL,
			  active tinyint NULL,
			  pdf text NULL,
			  image text NULL,
			  rohs text NULL,
			  multipart tinyint(1) NULL,
			  percentLoss float NULL,
			  inventoryDate datetime NULL,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}
	else{
		echo "table already $table_name exists<br>";
	}

	$table_name = $wpdb->prefix.'warehouse_multipart';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  id_parent int NOT NULL,
			  id_part int NOT NULL,
			  quantity float NOT NULL,
			  designator text NOT NULL,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}
	else{
		echo "table already $table_name exists<br>";
	}

	$table_name = $wpdb->prefix.'warehouse_backup';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  package text NOT NULL,
			  qty10 int NOT NULL,
			  qty25 int NOT NULL,
			  qty50 int NOT NULL,
			  qty100 int NOT NULL,
			  qty200 int NOT NULL,
			  qty500 int NOT NULL,
			  qty1000 int NOT NULL,
			  qty2000 int NOT NULL,
			  qty5000 int NOT NULL,
			  qty10000 int NOT NULL,
			  qtyAbove int NOT NULL,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}
	else{
		echo "table already $table_name exists<br>";
	}

	$table_name = $wpdb->prefix.'warehouse_settings';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  name text NOT NULL,
			  value text,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}

	$table_name = $wpdb->prefix.'warehouse_parts_log';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  partId int NOT NULL,
			  quantityChange int NOT NULL,
			  quantity int NOT NULL,
			  price float,
			  currency text,
			  warehousemanId int NOT NULL,
			  warehousemanName text,
			  receipt text,
			  supplier text,
			  insertionDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}
	else{
		echo "table already $table_name exists<br>";
	}

	$table_name = $wpdb->prefix.'warehouse_hashes';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  md5 text NOT NULL,
			  rowID int NOT NULL,
			  db text NOT NULL,
			  insertionDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}
	else{
		echo "table already $table_name exists<br>";
	}

	$table_name = $wpdb->prefix.'warehouse_address';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  company text NOT NULL,
			  street text NOT NULL,
			  street2 text NULL,
			  street3 text NULL,
			  houseNumber text NOT NULL,
			  city text NOT NULL,
			  postal text NOT NULL,
			  state text NULL,
			  country text NOT NULL,
			  contactPerson text NOT NULL,
			  contactEmail text NOT NULL,
			  contactTel text NULL,
			  type text NOT NULL,
			  additionalInfo  text NULL,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}
	else{
		echo "table already $table_name exists<br>";
	}

	$table_name = $wpdb->prefix.'warehouse_order_parts_cart';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  id_part int NOT NULL,
			  id_user int NOT NULL,
			  quantity float NOT NULL,
			  purpose text,
			  state int DEFAULT 0,
			  supplierID int, 
			  estimatedDate date,
			  orderNumber int,
			  insertionDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}
	else{
		echo "table already $table_name exists<br>";
	}

	$table_name = $wpdb->prefix.'warehouse_manufacture_plan';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		 $charset_collate = $wpdb->get_charset_collate();
	  
		 $sql = "CREATE TABLE $table_name (
			  id int(11) NOT NULL AUTO_INCREMENT,
			  id_part int NOT NULL,
			  id_user int NOT NULL,
			  quantity float NOT NULL,			  
			  estimatedDate date,
			  state int DEFAULT 0,
			  insertionDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  UNIQUE KEY id (id)
		 ) $charset_collate;";
		 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		 dbDelta( $sql );
		 echo "Table $table_name created  <br>";
	}
	else{
		echo "table already $table_name exists<br>";
	}

	$current_user = wp_get_current_user();
	$user_permission = get_user_meta( $current_user->ID, 'warehouse_permission' );
	if( empty($user_permission) )
		add_user_meta( $current_user->ID, 'warehouse_permission', 'super', false );
	
    echo "<b>databases installation finished</b><br>";
}

function install_warehouse_plugin_pages() {
	$current_user = wp_get_current_user();

	if ( $post = get_page_by_title( 'warehouse', '', 'page' ) )
		$id = $post->ID;
	else
		$id = wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'Warehouse' ),
		'post_content'  => '[warehouse_list_of_components]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	  	=> 'warehouse'
		));
	
	if ( get_page_by_path( 'warehouse/edit_component' ) == NULL)
		wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'Edit components' ),
		'post_content'  => '[warehouse_edit_components]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	    => 'edit_component',
		'post_parent'   => $id
		));

	if ( get_page_by_path( 'warehouse/new_component' ) == NULL)
		wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'New component' ),
		'post_content'  => '[warehouse_new_component]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	    => 'new_component',
		'post_parent'   => $id
		));

	if ( get_page_by_path( 'warehouse/import_goods_to_warehouse' ) == NULL)
		wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'Import goods to the warehouse' ),
		'post_content'  => '[warehouse_import_goods_to_warehouse]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	    => 'import_goods_to_warehouse',
		'post_parent'   => $id
		));

	if ( get_page_by_path( 'warehouse/export_goods_from_warehouse' ) == NULL){
		wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'Export goods from the warehouse' ),
		'post_content'  => '[warehouse_export_goods_from_warehouse]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	    => 'export_goods_from_warehouse',
		'post_parent'   => $id
		));
		createSettingsValue('EXPORT_RECEIPT_NUMBER', "1");
		createSettingsValue('IMPORT_RECEIPT_NUMBER', "1");
	}

	if ( get_page_by_path( 'warehouse/list_of_multiparts' ) == NULL)
		wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'List of multipart' ),
		'post_content'  => '[warehouse_list_of_multiparts]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	    => 'list_of_multiparts',
		'post_parent'   => $id
		));

	if ( get_page_by_path( 'warehouse/order_components' ) == NULL)
		wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'Order components' ),
		'post_content'  => '[warehouse_order_components]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	    => 'order_components',
		'post_parent'   => $id
		));

	if ( get_page_by_path( 'warehouse/cart' ) == NULL)
		wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'Cart' ),
		'post_content'  => '[warehouse_cart]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	    => 'cart',
		'post_parent'   => $id
		));

	if ( get_page_by_path( 'warehouse/manufacture' ) == NULL)
		wp_insert_post(array(
		'post_title'    => wp_strip_all_tags( 'Manufacture parts' ),
		'post_content'  => '[warehouse_manufacture]',
		'post_status'   => 'publish',
		'post_author'   => $current_user->ID,
		'post_type'     => 'page',
		'post_name'	    => 'manufacture',
		'post_parent'   => $id
		));
}

?>