<?php

/**
 * Template Name: List of Parts
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}
include_once  WAREHOUSE_DIR_SERVER."inc/common/functions.php";

// return number of sessions
function warehouse_list_of_multiparts($atts) { 
	global $wpdb;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );

	?>
	<form action="" method="get"  class="stockCSS">
		<div class="row">
			<div class="four_columns">
				<div>
					<label for="quantity" id="quantity_label">Množství: <font color="red">*</font></label>
					<input id="quantity" min="1" type="number" name="quantity" value="<?php if (isset($_GET['quantity'])) echo strtoupper($_GET['quantity']); else echo "1"; ?>" autofocus required/>
				</div>
			</div>

			<div class="two_columns">
				<div class='warehouseItem'>
					<label for="detailed">Detail</label>
					<label class='switch'><input id="detailed" type="checkbox" name="detailed" <?php if (isset($_GET['detailed'])) echo 'checked'; ?> > <span class='slider round'></span></label>
				</div>

				<div class='warehouseItem'>
					<label for="toOrder">Pro objednavku</label>
					<label class='switch'><input id="toOrder" type="checkbox" name="toOrder" <?php if (isset($_GET['toOrder'])) echo 'checked'; ?> > <span class='slider round'></span></label>
				</div>
				
				<div class='warehouseItem'>
					<label for="subtractOnStock">Snížit o součástky na skladě</label>
					<label class='switch'><input id="subtractOnStock" type="checkbox" name="subtractOnStock" <?php if (isset($_GET['subtractOnStock'])) echo 'checked'; ?> > <span class='slider round'></span></label>
				</div>
				
				<div class='warehouseItem'>
					<label for="addSKU">Přidat SKU</label>
					<label class='switch'><input id="addSKU" type="checkbox" name="addSKU" <?php if (isset($_GET['addSKU'])) echo 'checked'; ?> > <span class='slider round'></span></label>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="four_columns">
				<button name="part_id" value="<?php echo $_GET['part_id'];?>" type="submit" style="float: left;">Změnit</button>
			</div>
		</div>

	</form>
	
	<br>

	<?php

	// read list of parts
	$table_name = $wpdb->prefix.'warehouse_multipart';
	$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id_parent`=".$_GET['part_id']);
	$parts = $results;



	if(isset($_GET['quantity']))
		$loadedParts = addMultiPart($_GET['part_id'], $_GET['quantity']);
	else
		$loadedParts = addMultiPart($_GET['part_id'], 1);
	

	$table_name = $wpdb->prefix.'warehouse_parts';
	$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$_GET['part_id']);
	$part = $results;

	echo "<h2>".$part[0]->value."</h2>";

	$simplified = 0;
	$detailed = 0;
	if(isset($_GET['toOrder'])){
		$simplified = 1;
	}

	if(isset($_GET['detailed'])) {
		$detailed = 1;
	}

	$subOnStock = 0;
	if(isset($_GET['subtractOnStock']))
		$subOnStock = 1;

	$addSKU = 0;
	if(isset($_GET['addSKU']))
		$addSKU = 1;

	$prices = showMultiParts(array( 0 => $loadedParts), 0, $detailed, $simplified, $subOnStock, $addSKU);
}

add_shortcode('warehouse_list_of_multiparts', 'warehouse_list_of_multiparts');
