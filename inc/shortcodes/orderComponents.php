<?php

/**
 * Template Name: Create order of components/schedule production
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';

// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}
include_once  WAREHOUSE_DIR_SERVER."inc/common/definitions.php";

function warehouse_create_order($atts) { 
	global $wpdb;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );

	$current_user = wp_get_current_user();
	
	if(!isset($_SESSION['order_step'])){
		$_SESSION['order_step'] = 1;
	}
	
	if(isset($_POST['btnCheck']) | isset($_SESSION['order_scannedParts'])){		
		if(isset($_POST['btnCheck']))
			$_SESSION['order_scannedParts'] = $_POST['scannedParts'];
		
		$decodedParts = array();
		$allValid = true;
		$receiptNumber = intval(getSettingsValue("EXPORT_RECEIPT_NUMBER"));
		$receiptTextNumber = str_pad($receiptNumber, 8, '0', STR_PAD_LEFT);

		foreach(preg_split("/((\r?\n)|(\r\n?))/", $_SESSION['order_scannedParts']) as $line){
			if($line == "")
				continue;
			
			$fields = explode (";", $line); 
			
			if(count($fields) >= 2){
				// get current quantity of the goods
				$table_name = $wpdb->prefix.'warehouse_parts';
				$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `partname`='".trim($fields[0])."'");
				
				if(!empty($results)){
					$part = $results[0];
					
					if(count($fields) == 2){
						$x = array_search($part->partname, array_column($decodedParts, 'name'));
						if($x !== false){
							$decodedParts[$x]['quantity'] += $fields[1];
						}
						else
							array_push($decodedParts, array('id'			=> $part->id, 
                                                            'name'          => $part->partname,
                                                            'stockQuantity' => $part->quantity,
															'quantity' 		=> intval($fields[1]), 
                                                            'description'	=> $part->description, 
                                                            'price' 		=> $part->price, 
                                                            'currency'		=> $part->currency, 
															'valid' 		=> 1
							));
					}
				}	
			}
		} 
		
		$_SESSION['order_step'] = 2;
		$_SESSION['order_decodedParts'] = $decodedParts;
		$_SESSION['order_noIDs'] = count($decodedParts);
	}
	
	if(isset($_POST['btnSave'])){
		$parts = $_SESSION['order_decodedParts'];
        // assign order number to log the order
        $receiptNumber = intval(getSettingsValue("ORDER_NUMBER"));
        $receiptTextNumber = str_pad($receiptNumber, 8, '0', STR_PAD_LEFT);

		for($i=1; $i<=$_SESSION['order_noIDs']; ++$i){
			$text = 'quantity'.$i;
			$quantity = intval($_POST[$text]);
			
            $text = 'id'.$i;
			$partID = $_POST[$text];
			
			// get current quantity of the goods
			$table_name = $wpdb->prefix.'warehouse_order_parts_cart';
            $data  = array( 'id_part'				=> $partID, 
                            'quantity'				=> $quantity, 
                            'id_user'		        => $current_user->ID,
                            'purpose'               => $_POST['purpose'],
							'state'					=> constant("IN_CART")
            );
            $wpdb->insert($table_name, $data);					
		}

		$_SESSION['order_step'] = 3;
		unset($_SESSION['order_decodedParts']);
		unset($_SESSION['order_noIDs']);
		unset($_SESSION['order_scannedParts']);
	}
	
	if(isset($_POST['btnBack'])){
		--$_SESSION['order_step'];
	}
	
?>


<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<div class="wrap">
			<?php 
			switch($_SESSION['order_step']){
				case 1:
			?>
				
				<div align='center'>
					<button class="tablinkSel" id="scan" style="float: center;">Skenovat</button>
					<button class="tablink" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				
				<div>
					<form action="" method="post"  class="stockCSS">
						<div class="one_column">
							<label for="scannedParts" id="name_label">Naskenované součástky a jejich množství: <font color="red">*</font></label>
							<textarea id="scannedParts" rows="4" cols="50" name="scannedParts" required autofocus><?php if(isset($_SESSION['order_scannedParts'])) echo $_SESSION['order_scannedParts'] ?></textarea>
						</div>
						<div>
							<b>Format:</b> Barcode;Quantity
						</div>
						<div class="one_column">
							<button name="btnCheck" type="submit" style="float: right;">Zkontrolovat</button>
						</div>
					</form>
				</div>
			<?php
				break;
				case 2:
			?>
				<div align='center'>
					<button class="tablink" id="scan" style="float: center;">Skenovat</button>
					<button class="tablinkSel" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				
				<form action="" method="post"  class="stockCSS">
					<div class="row">
						<div class="two_columns">
							<div>
								<label>Uživatel:</label>
								<label class='subLabel'><?php echo $current_user->display_name ?></label>
							</div>
						</div>
						<div class="two_columns">
							<div>
							    <label>Účel: <font color="red">*</font></label>
								<input id="purpose" type="text" name="purpose" value="<?php if(isset($_POST['purpose'])) echo $_POST['purpose']; ?>" required autofocus />
							</div>
						</div>
					</div> 
					
					<table class="stockCSS">
					<tr class='headerRow'>
						<th>Název</th>
						<th class='thCenter'>Na skladě<br/>Na objednávce</th>
						<th>Kupované množství</th>
						<th>Cena</th>
						<th>Popis</th>
					</tr>
					<?php
						$id = 0;
						foreach($decodedParts as $part){
							++$id;									
								if($part['valid'] == 0){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"2\">".$part['name']."</td>";
									echo "<td>".$part['quantity']."</td>";
									echo "<td>".$part['description']."</td>";
								}
								else if($part['valid'] == -1){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"6\">".$part['text']."</td>";
								}
								else{
									echo "<tr>";
									echo "<td>".$part['name']."<input style='display:none;' type='number' name='id".$id."' value='".intval($part['id'])."'\"></td>";
									echo "<td class='tdCenter'>".$part['stockQuantity']."</td>";
									echo "<td><input type='number' min='0' "."' name='quantity".$id."'  value='".intval($part['quantity'])."'></td>";
                                    echo "<td>".$part['price']." ".$part['currency']."</td>";
									echo "<td>".$part['description']."</td>";
								}
								echo "</tr>";
							echo "</form>";
						}
					?>
					
					</table>

					<div class="one_column">
						<script>
							function removeRequired(form){
								$.each(form, function(key, value) {
									if ( value.hasAttribute("required")){
										value.removeAttribute("required");
									}
								});
							}
						</script>
						<?php if($allValid == true)
							echo "<button name='btnSave' type='submit' style='float: right;'>Do košíku</button>";
						?>	
						<button name="btnBack" type="submit" style="float: left;" onClick="removeRequired(this.form)">Zpět</button>
					</div>
				</form>
			
			<?php
				break;
				case 3:
			?>
				<div align='center'>
					<button class="tablink" id="scan" style="float: center;">Skenovat</button>
					<button class="tablink" id="check" style="float: center;">Kontrola</button>
					<button class="tablinkSel" id="save" style="float: center;">Uložit</button>
				</div>
				
				<h2 class="stockCSS"><font color="green">Záznam uložen</font></h2>
				
				<form action="" method="post" class="stockCSS">
					<div class="one_column">
						<button name="btnNew" type="submit" style="float: right;">Nový záznam</button>
					</div>
				</form>
			<?php
				unset($_SESSION['order_step']);
				break;
			}
			?>

</div><!-- .wrap -->

<?php
}
add_shortcode('warehouse_order_components', 'warehouse_create_order'); 