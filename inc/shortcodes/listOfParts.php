<?php

/**
 * Template Name: List of Parts
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// return number of sessions
function warehouse_list_of_components($atts) { 
	global $wpdb;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );

	// read list of parts
	$table_name = $wpdb->prefix.'warehouse_parts';
	$results = $wpdb->get_results("SELECT * FROM $table_name ORDER BY `type` ASC, `partname` ASC");
	$parts = $results;
	
	?>

<script>
function searchInTable() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("txtSearch");
  filter = input.value.toUpperCase();
  table = document.getElementById("warehouseTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td1 = tr[i].getElementsByTagName("td")[1];
	td2 = tr[i].getElementsByTagName("td")[2];
    if (td1) {
      txtValue = td1.textContent || td1.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
		if(td2){
		  txtValue = td2.textContent || td2.innerText;
		  if (txtValue.toUpperCase().indexOf(filter) > -1) {
			tr[i].style.display = "";
		  } else {
			tr[i].style.display = "none";
		  }
		}
      }
    } 
    	
  }
}
</script>

<div class='one_column'>
	<?php
	if(isset($_SESSION['warehouse_message'])){
		echo $_SESSION['warehouse_message'];
		unset($_SESSION['warehouse_message']);
	} 
	if(isset($_GET["detailID"])){
		viewPartDetails($_GET["detailID"]);
	}
	else{
	?>
	<input type="text" id="txtSearch" onkeyup="searchInTable()" class='warehouse' placeholder="Hledej název/čárový kód/popis součástky ..." title="Type in a name" autofocus>	
	<table class="warehouse" id='warehouseTable'>
	<tr class='headerRow'>
		<th>Typ</th>
		<th><b>Název</b></th>
		<th><b>Krátký popis</b></th>
		<th class="thCenter"><b>Umístění</b></th>
		<th class="thCenter"><b>Množství</b></th>
		<th class="thCenter"><b>Cena</b></th>
		<th>DOC</th>
	</tr>
	<?php
		$i=1;
		
		foreach($parts as $part){
			if(!$part->active) continue;

			if($i%2)
				echo "<tr style='background-color:#eeeeee'>";
			else
				echo "<tr>";
				
			echo "<td>".$part->type."</td>";
			$url = get_permalink( get_page_by_path( 'warehouse' ));

			echo "<td><b><a href='".$url."?detailID=".$part->id."'>".$part->partname."</a></b>";
			if($part->sku != ""){
				echo "<font color='#a7a7a7'><br/><i>SKU: ".$part->sku."<i></font>";
			}
			echo "</td>";
			echo "<td>".$part->description."<br/><font color='#a7a7a7'><i>MFG: ".$part->manufacturer."</i></font></td>";
			echo "<td align='center'><b><font color='".$part->posColour."'>".$part->position."</font></b></td>";
			echo "<td align='center'>".$part->quantity."</td>";
			if($part->multipart){
				$homeCurrency = getSettingsValue("CURRENCY_HOME");
				$partPrice = calculateTotalPartPrice($part->id);
				echo "<td align='right'>".round($partPrice,3)." ".$homeCurrency."</td>";
			}
			else{
				echo "<td align='right'>".round($part->price, 3)." ".$part->currency."</td>";
			}
			echo "<td align='center'>";
			if($part->pdf != "")
				echo "<a class='dashicons dashicons-pdf' href='".$part->pdf."'/>";
			
			if($part->image != "")
				echo "<a class='dashicons dashicons-format-image' href='".$part->image."'/>";

			if($part->rohs != "")
				echo "<a class='dashicons dashicons-format-aside' href='".$part->rohs."'/>";

			if($part->multipart){
				$url = get_permalink( get_page_by_path( 'warehouse/list_of_multiparts' ));
				echo "<a class='dashicons dashicons-editor-ul' href='".$url."?part_id=".$part->id."'/>";
			}
				echo "</td>";
			echo "</tr>";
			++$i;
		}
		?>
	</table>
	<?php
	}
	?>
</div><!-- .wrap -->

<?php
}

add_shortcode('warehouse_list_of_components', 'warehouse_list_of_components');

