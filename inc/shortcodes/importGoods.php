<?php

/**
 * Template Name: Import goods to the warehouse
 *
 * $_SESSION['step'] - positon in the 
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';

function warehouse_import_goods_to_warehouse($atts) { 
	global $wpdb;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );

	$current_user = wp_get_current_user();
   /* echo 'Username: ' . $current_user->user_login . '<br />';
    echo 'User email: ' . $current_user->user_email . '<br />';
    echo 'User first name: ' . $current_user->user_firstname . '<br />';
    echo 'User last name: ' . $current_user->user_lastname . '<br />';
    echo 'User display name: ' . $current_user->display_name . '<br />';
    echo 'User ID: ' . $current_user->ID . '<br />';*/

	if(!isset($_SESSION['step'])){
		$_SESSION['step'] = 1;
	}

	$receiptNumber = intval(getSettingsValue("IMPORT_RECEIPT_NUMBER"));
	$receiptTextNumber = str_pad($receiptNumber, 8, '0', STR_PAD_LEFT);
	
	if(isset($_POST['btnCheck']) | isset($_SESSION['scannedParts'])){
		if(isset($_POST['btnCheck']))
			$_SESSION['scannedParts'] = $_POST['scannedParts'];
			
		$decodedParts = array();
		$allValid = true;
		foreach(preg_split("/((\r?\n)|(\r\n?))/", $_SESSION['scannedParts']) as $line){
			if($line == "")
				continue;
			$fields = explode (";", $line);  
			
			if(count($fields) >= 2){
				$table_name = $wpdb->prefix.'warehouse_parts';
				$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `partname`='".$fields[0]."'");

				if(!empty($results)){
					$part = $results[0];
					
					if(count($fields) == 2)
						array_push($decodedParts, array('id'			=> $part->id, 
														'onStock' 		=> $part->quantity, 
														'name' 			=> $part->partname, 
														'quantity' 		=> $fields[1], 
														'description'	=> $part->description, 
														'valid' 		=> 1							
						));
					else
						array_push($decodedParts, array('id'			=> $part->id,
														'onStock' 		=> $part->quantity, 
														'name' 			=> $part->partname, 
														'quantity' 		=> $fields[1], 
														'price' 		=> $fields[2], 
														'currency'		=> $fields[3], 
														'description'	=> $part->description, 
														'valid'			=> 1
						));
				}
				else{
					$allValid = false;
					if(count($fields) == 2)
						array_push($decodedParts, array('name' 		=> $fields[0], 
														'quantity' 	=> $fields[1], 
														'valid' 	=> 0
						));
					else
						array_push($decodedParts, array('name' 		=> $fields[0], 
														'quantity' 	=> $fields[1], 
														'price' 	=> $fields[2], 
														'currency'	=> $fields[3], 
														'valid'		=>0
						));
				}
			}
			else{
				$allValid = false;
				array_push($decodedParts, array('text' => $line, 'valid'=>-1));
			}
		} 
		
		$_SESSION['step'] = 2;
		$_SESSION['decodedParts'] = $decodedParts;
		$_SESSION['noIDs'] = count($decodedParts);
	}
	
	if(isset($_POST['btnSave'])){
		$parts = $_SESSION['decodedParts'];
		for($i=0; $i<$_SESSION['noIDs']; ++$i){
			$text = 'quantity'.$i;
			$quantity = intval($_POST[$text]);
			$text = 'price'.$i;
			$price = floatval($_POST[$text]);
			$text = 'currency'.$i;
			$currency = $_POST[$text];
			$text = 'id'.$i;
			$partID = $_POST[$text];

			// get current quantity of the goods
			$table_name = $wpdb->prefix.'warehouse_parts';
			$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$partID);
			$newQty = $quantity + $results[0]->quantity;

			// update quantity of the goods
			$table_name = $wpdb->prefix.'warehouse_parts';
			$data = 	array(	'quantity' 			=> $newQty,
								'price'				=> $price, 
								'currency'			=> $currency,
								'currentSupplier'	=> $_POST['supplier']

			);			
			$where = 	array('id' 			=> $partID);
			$wpdb->update( $table_name, $data, $where);

			// log of the received goods
			$receiptNumber = intval(getSettingsValue("IMPORT_RECEIPT_NUMBER"));
			$receiptTextNumber = str_pad($receiptNumber, 8, '0', STR_PAD_LEFT);
			$table_name = $wpdb->prefix.'warehouse_parts_log';
			$data  = array( 'partId'				=> $partID, 
							'quantityChange'		=> $quantity, 
							'quantity'				=> $newQty,
							'price'					=> $price, 
							'currency'				=> $currency, 
							'warehousemanId'		=> $current_user->ID, 
							'warehousemanName'		=> $current_user->display_name, 
							'receipt'				=> "I".$receiptTextNumber,
							'supplier'				=> $_POST['supplier']
			);
			$wpdb->insert($table_name, $data);
			
		}
		$receiptNumber = intval(getSettingsValue("IMPORT_RECEIPT_NUMBER"));
		updateSettingsValue("IMPORT_RECEIPT_NUMBER", $receiptNumber+1);

		$_SESSION['step'] = 3;
		unset($_SESSION['decodedParts']);
		unset($_SESSION['noIDs']);
		unset($_SESSION['scannedParts']);
	}
	
	if(isset($_POST['btnBack'])){
		--$_SESSION['step'];
	}
	
?>


<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<h1 class="stockCSS">Nová příjemka</h1>
			<?php 
			switch($_SESSION['step']){
				case 1:
			?>
				<div align='center'>
					<button class="tablinkSel" id="scan" style="float: center;">Skenovat</button>
					<button class="tablink" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				<div>
					<form action="" method="post" class="stockCSS">
						<div class="one_column">
							<label for="scannedParts" id="name_label">Naskenované součástky a jejich množství (cena a měna): <font color="red">*</font></label>
							<textarea id="scannedParts" rows="4" cols="50" name="scannedParts" required autofocus><?php if(isset($_SESSION['scannedParts'])) echo $_SESSION['scannedParts']; ?></textarea>
						</div>
						<div>
							<b>Format:</b> Barcode;Quantity[;Price; Currency]
						</div>
						<div class="one_column">
							<button name="btnCheck" type="submit" style="float: right;">Načíst</button>
						</div>
					</form>
				</div>
			<?php
				break;
				case 2:
			?>
				<div align='center'>
					<button class="tablink" id="scan" style="float: center;">Skenovat</button>
					<button class="tablinkSel" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				
				<form action="" method="post" class="stockCSS">
							
					<div class="row">
						<div class="two_columns">
							<div>
								<label>Skladník:</label>
								<label class='subLabel'><?php echo $current_user->display_name ?></label>
							</div>
						</div>
						<div class="two_columns">
							
						</div>
					</div>
					<div class="row">
						<div class="two_columns">
							<div>
								<label>Dodavatel:  <font color="red">*</font></label>
								<input id="supplier" type="text" name="supplier" value="<?php if(isset($_POST['supplier'])) echo $_POST['supplier']; ?>" required autofocus />
							</div>
						</div>
						<div class="two_columns">
							<div>
								<label>Příjemka: </label>
								<label class='subLabel'><?php echo "I".$receiptTextNumber ?> </label>
							</div>
						</div>
					</div>

					<hr />
					<div></div>
					<table class="stockCSS">
					<tr class='headerRow'>
						<th>Název</th>
						<th class='thCenter'>Skladem</th>
						<th>Množství<font color="red">*</font></th>
						<th>Cena<font color="red">*</font></th>
						<th>Měna<font color="red">*</font></th>
						<th>Popis</th>
					</tr>
					<?php
						$id = 0;
						foreach($decodedParts as $part){	
								if($part['valid'] == 0){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"6\">".$part['name']."</td>";
									echo "<td>".$part['quantity']."</td>";
									echo "<td>".$part['price']."</td>";
									echo "<td>".$part['currency']."</td>";
									echo "<td>".$part['description']."</td>";
								}
								else if($part['valid'] == -1){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"6\">".$part['text']."</td>";
								}
								else{
									if(!isset($part['price']))
										$part['price'] = "";
									if(!isset($part['currency']))
										$part['currency'] = "";

									echo "<tr>";
									echo "<td>".$part['name']."<input style='display:none;' type='number' name='id".$id."' value='".intval($part['id'])."'\"></td>";
									echo "<td align='center'>".$part['onStock']."</td>";
									echo "<td><input required autofocus type='number' min='0' name='quantity".$id."'  value='".intval($part['quantity'])."'></td>";
									echo "<td><input required autofocus type='number' min='0' step=any name='price".$id."' value='".$part['price']."'></td>";
									echo "<td><input required autofocus type='text' name='currency".$id."' value='".$part['currency']."'></td>";
									echo "<td>".$part['description']."</td>";
								}
								echo "</tr>";
							echo "</form>";
							++$id;
						}
					?>
					
					</table>

					<div class="one_column">
						<script>
							function removeRequired(form){
								$.each(form, function(key, value) {
									if ( value.hasAttribute("required")){
										value.removeAttribute("required");
									}
								});
							}
						</script>
						
						<?php if($allValid == true)
							echo "<button name=\"btnSave\" type=\"submit\" style=\"float: right;\">Přijmout</button>";
						?>	
						<button name="btnBack" id="btnBack" style="float: left;" onClick="removeRequired(this.form)">Zpět</button>
					</div>
				</form>
			
			<?php
				break;
				case 3:
			?>
					<div align='center'>
						<button class="tablink" id="scan" style="float: center;">Skenovat</button>
						<button class="tablink" id="check" style="float: center;">Kontrola</button>
						<button class="tablinkSel" id="save" style="float: center;">Uložit</button>
					</div>
					
					<h2 class="stockCSS"><font color="green">Záznam uložen</font></h2>
					
					<form action="" method="post" class="stockCSS">
						<div class="one_column">
							<button name="btnNew" type="submit" style="float: right;">Nový záznam</button>
						</div>
					</form>
			<?php
				unset($_SESSION['step']);
				break;
			}
			?>


</div><!-- .wrap -->

<?php
}
add_shortcode('warehouse_import_goods_to_warehouse', 'warehouse_import_goods_to_warehouse'); 