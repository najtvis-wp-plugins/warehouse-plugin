<?php

/**
 * Template Name: Edit parts
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';

// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}
include_once  WAREHOUSE_DIR_SERVER."inc/common/functions.php";

function warehouse_edit_components($atts) { 
	global $wpdb;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );

	$current_user = wp_get_current_user();
	$errorSave = false;
	$message = "";
	if(isset($_POST['savePart'])){
		$table_name = $wpdb->prefix.'warehouse_parts';

		if(findMD5hash($_POST['savePart'], $table_name, $_SESSION['editedPartMD5'])){
			if(isset($_POST['activePart']))
				$activePart = true;
			else
				$activePart = false;
			
			if($_POST['replacement'] != ""){
				$replacementPartsList = explode (";", $_POST['replacement']);
				foreach($replacementPartsList as $listedPart){ 
					$table_name = $wpdb->prefix.'warehouse_parts';
					$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `partname`='".$listedPart."'");
					
					if(empty($results)){
						$replacementPart = $results[0];				
						$message = $message."Náhrada '".strtoupper($listedPart)."' nebyla v seznamu nalezena<br>";
						$errorSave = true;
					}
				}
			}
			if($_POST['type'] == ""){
				$message = $message."You have to specify part type <br/>";
				$errorSave = true;
			}

			if($_POST['sku'] != ""){
				$table_name = $wpdb->prefix.'warehouse_parts';
				$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `sku`='".$_POST['sku']."'");
				
				if(!empty($results) && $results[0]->id != $_SESSION['editedPart']){
					$replacementPart = $results[0];				
					$message = $message."SKU '".strtoupper($_POST['sku'])."' již v seznamu existuje u součástky s partname: ".$results[0]->partname."<br>";
					$errorSave = true;
				}
			}

			if($errorSave == false){
				$data = 	array(	'active' 			=> 	$activePart,
									'manufacturer'		=>	$_POST['manufacturer'],
									'package'			=>	$_POST['package'],
									'details'			=>	$_POST['details'],
									'description'		=>	$_POST['short_desc'],
									'position'			=>	$_POST['position'],
									'minQuantity'		=>	$_POST['min_qty'],
									'quantityPerBox'	=>	$_POST['quantityPerBox'],
									'boxPackageType'	=>	$_POST['boxPackageType'],
									'currentSupplier'	=>	$_POST['supplier'],
									'posColour'			=>	$_POST['posColour'],
									'leadTime'			=>	$_POST['lead_time'],
									'replacement'		=>	$_POST['replacement'],
									'type'				=>	$_POST['type'],
									'price'				=>	$_POST['price'],
									'currency'			=>	$_POST['currency'],
									'value'				=>  $_POST['human_readable'],
									'percentLoss'		=>  $_POST['percentLoss'],
									'sku'				=>  $_POST['sku']
				);
				$table_name = $wpdb->prefix.'warehouse_parts';
				$where = 	array('id' 			=> $_SESSION['editedPart']);
				$wpdb->update( $table_name, $data, $where);

				$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$_SESSION['editedPart']);
				$message = "Položka ".$results[0]->partname." uložena";
				deleteMD5hash($_POST['savePart'], $table_name, $_SESSION['editedPartMD5']);
				unset($_SESSION['editedPart']);
				unset($_SESSION['editedPartMD5']);
			}
			unset($_POST['savePart']);
		}
		else{
			$message = $message."Chyba - Položku nelze uložit, zkuste operaci opakovat<br>";
			$errorSave = true;
			unset($_POST['savePart']);
			unset($_SESSION['editedPart']);
			unset($_SESSION['editedPartMD5']);
		}
	}
	else if(isset($_POST['deletePart'])){
		$table_name = $wpdb->prefix.'warehouse_parts';

		if(findMD5hash($_POST['deletePart'], $table_name, $_SESSION['editedPartMD5'])){
			$where = 	array('id' 			=> $_SESSION['editedPart']);
			$wpdb->delete( $table_name, $where);
			$message = "Položka Smazána";
			$errorSave = true;
			deleteMD5hash($_POST['deletePart'], $table_name, $_SESSION['editedPartMD5']);
			unset($_SESSION['editedPart']);
			unset($_POST['deletePart']);
		}
		else{
			$message = $message."Chyba - Položku nelze smazat, zkuste operaci opakovat<br>";
			$errorSave = true;
		}
	}
	else if(isset($_POST['changeQauntity'])){
		$current_user = wp_get_current_user();
		$is_admin = get_user_meta( $current_user->ID, 'warehouse_is_admin', true);
		if($is_admin == '0'){
			$message = "Nemáte právo měnit množství na skladě";
			$errorSave = true;
			unset($_POST['changeQauntity']);
		}	
		else{
			// read edited part
			if(isset($_POST['changeQauntity'])){
				$_SESSION['changeQauntity'] = $_POST['changeQauntity'];
				$_SESSION['changeQauntityMD5'] = generateMD5();
				$table_name = $wpdb->prefix.'warehouse_hashes';
				insertMD5hash($_POST['changeQauntity'], $wpdb->prefix.'warehouse_parts', $_SESSION['changeQauntityMD5']);
			}

			$table_name = $wpdb->prefix.'warehouse_parts';
			$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$_SESSION['changeQauntity']);
			if(!empty($results))
				$editedPart = $results[0];
		}		
	}
	else if(isset($_POST['saveChQTY'])){
		$table_name = $wpdb->prefix.'warehouse_parts';

		if(isset($_SESSION['changeQauntityMD5']) && findMD5hash($_POST['saveChQTY'], $table_name, $_SESSION['changeQauntityMD5'])){
			if(isset($_POST['activePart']))
				$activePart = true;
			else
				$activePart = false;

			$table_name = $wpdb->prefix.'warehouse_parts';
			$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$_POST['saveChQTY']);
			$part = $results[0];

			$qtyChange = intval($_POST['quantity']) - $part->quantity;

			$table_name = $wpdb->prefix.'warehouse_parts_log';
			$data  = array( 'partId'				=> $part->id, 
							'quantityChange'		=> $qtyChange, 
							'quantity'				=> $_POST['quantity'],
							'price'					=> $part->price, 
							'currency'				=> $part->currency, 
							'warehousemanId'		=> $current_user->ID, 
							'warehousemanName'		=> $current_user->display_name, 
							'receipt'				=> "MANUAL"
			);
			$wpdb->insert($table_name, $data);

			$table_name = $wpdb->prefix.'warehouse_parts';
			$data = 	array( 	'quantity' 			=> 	$_POST['quantity'],
								'inventoryDate'		=>	date('Y-m-d H:i:s')
			);
			$where = 	array('id' 			=> 	$_SESSION['changeQauntity']);
			$wpdb->update( $table_name, $data, $where);

			$message = "Položka uložena";
			deleteMD5hash($_POST['saveChQTY'], $table_name, $_SESSION['changeQauntityMD5']);
			unset($_SESSION['changeQauntity']);
			unset($_SESSION['changeQauntityMD5']);
			unset($_POST['saveChQTY']);
		}
		else{
			$message = $message."Chyba - Položku nelze uložit, zkuste operaci opakovat<br>";
			$errorSave = true;
		}
	}
	else if(isset($_POST['btnBackChQTY'])){
		$table_name = $wpdb->prefix.'warehouse_parts';

		deleteMD5hash($_SESSION['changeQauntity'], $table_name, $_SESSION['changeQauntityMD5']);
		unset($_SESSION['changeQauntity']);
		unset($_SESSION['changeQauntityMD5']);
		unset($_POST['btnBackChQTY']);
	}

	if(isset($_POST['editPart']) || isset($_SESSION['editedPart'])){
		$errorLoad = 0;
		// read edited part
		if(isset($_POST['editPart'])){
			$_SESSION['editedPart'] = $_POST['editPart'];
			$_SESSION['editedPartMD5'] = generateMD5();
			$table_name = $wpdb->prefix.'warehouse_hashes';
			insertMD5hash($_POST['editPart'], $wpdb->prefix.'warehouse_parts', $_SESSION['editedPartMD5']);
		}
		
		$table_name = $wpdb->prefix.'warehouse_parts';
		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$_SESSION['editedPart']);
		
		if(empty($results)){
			unset($_SESSION['editedPartMD5']);
			unset($_SESSION['editedPart']);
			$errorLoad = 1;
			$message = "Error when loading part, please another one";
		}
		else{
			$editedPart = $results[0];
		
			if($editedPart->active)
				$activePart = "checked='checked'";
			else
				$activePart = "";

			if($editedPart->multipart){
				$listOfParts = addMultiPart($_SESSION['editedPart'], 1);
			}
		}
	}
	
	else if(isset($_POST['uploadToServer'])){
		$uploadOk = 1;
		$message = "";
		//Check if the directory already exists.
		$directoryName = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/warehouse/parts/".$_SESSION['uploadType'];
		if(!is_dir($directoryName)){
			//Directory does not exist, so lets create it. 
			mkdir($directoryName, 0755, true);
		}
			
		// Allow certain file formats
		$imageFileType = strtolower(pathinfo(basename($_FILES["fileToUpload"]["name"]),PATHINFO_EXTENSION));
		if(!($imageFileType == "pdf" || $imageFileType == "jpg")) {
			$message = $message."Sorry, only ".$imageFileType." file is allowed. <br>";
			$uploadOk = 0;
		}
		
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			$_SESSION["messageUpload"] = -1;
			$message = $message."Sorry, your file was not uploaded. <br>";
		} 
		else {
			$table_name = $wpdb->prefix.'warehouse_parts';
			$results = $wpdb->get_results("SELECT * FROM $table_name WHERE id=".$_POST['uploadToServer'] );
			$part = $results[0];
			
			$filename = normalizeString($part->partname).".". $imageFileType;
			//if($_SESSION['uploadType'] == "ROHS")
				$filenameNEW = "/wp-content/uploads/warehouse/parts/".$_SESSION['uploadType']."/".$filename;
			if (rename($_FILES["fileToUpload"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $filenameNEW)) {
				chmod($_SERVER['DOCUMENT_ROOT'] . $filenameNEW, 0755);

				$table_name = $wpdb->prefix.'warehouse_parts';
				if(isset($_SESSION['uploadType'])){
					if($_SESSION['uploadType'] == "PDF")
						$data  = array( 'pdf' 				=> $filenameNEW);
					else if($_SESSION['uploadType'] == "IMG")
						$data  = array( 'image'				=> $filenameNEW);
					else if($_SESSION['uploadType'] == "ROHS")
						$data  = array( 'rohs'				=> $filenameNEW);

					$where = array('id' 				=> $_POST["uploadToServer"]);
					$wpdb->update($table_name, $data, $where);
					$_SESSION["messageUpload"] = 1;
				}
			} else {
				$_SESSION["messageUpload"] = -1;
				$message = "Sorry, there was an error uploading your file.";
			}
		}
		unset($_SESSION['uploadType']);
		if(isset($message))
			echo $message;
	}
	else if(isset($_POST['addPDF'])){
		// read edited part
		$table_name = $wpdb->prefix.'warehouse_parts';
		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$_POST['addPDF']);
		if(!empty($results))
			$editedPart = $results[0];

		$_SESSION['uploadType'] = 'PDF';
	}
	else if(isset($_POST['addROHS'])){
		// read edited part
		$table_name = $wpdb->prefix.'warehouse_parts';
		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$_POST['addROHS']);
		if(!empty($results))
			$editedPart = $results[0];

		$_SESSION['uploadType'] = 'ROHS';
	}
	else if(isset($_POST['addIMG'])){
		// read edited part
		$table_name = $wpdb->prefix.'warehouse_parts';
		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$_POST['addIMG']);
		if(!empty($results))
			$editedPart = $results[0];

		$_SESSION['uploadType'] = 'IMG';
	}

	// read list of parts
	$table_name = $wpdb->prefix.'warehouse_parts';
	$results = $wpdb->get_results("SELECT * FROM $table_name ORDER BY `type` ASC, `partname` ASC");
	$parts = $results;
	
?>


<script>
function searchInTable() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("txtSearch");
  filter = input.value.toUpperCase();
  table = document.getElementById("stockTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td1 = tr[i].getElementsByTagName("td")[1];
	td2 = tr[i].getElementsByTagName("td")[2];
    if (td1) {
      txtValue = td1.textContent || td1.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
		if(td2){
		  txtValue = td2.textContent || td2.innerText;
		  if (txtValue.toUpperCase().indexOf(filter) > -1) {
			tr[i].style.display = "";
		  } else {
			tr[i].style.display = "none";
		  }
		}
      }
    }  	
  }
}
</script>

<div  class='one_column'>		
	<?php if($errorSave == true)
		echo "<font color=\"red\">".$message."</font>";
	else
		echo "<font color=\"green\">".$message."</font>";
	?>
	
	<?php if(	!isset($_POST['editPart']) && 
				!isset($_POST['savePart']) && 
				!isset($_POST['addMultiParts']) && 
				!isset($_SESSION['createMultipartID']) &&
				!isset($_POST['addPDF']) && 
				!isset($_POST['addIMG']) &&
				!isset($_POST['addROHS']) &&
				!isset($_SESSION['editedPart']) &&
				!isset($_SESSION['changeQauntity']) 
			){ ?>
		<input type="text" id="txtSearch" class="warehouse" onkeyup="searchInTable()" placeholder="Hledej název/čárový kód/popis součástky ..." title="Type in a name" autofocus>	
		<table class="warehouse" id='stockTable'>
		<tr class='headerRow'>
			<th>Typ</th>
			<th><b>Název</b></th>
			<th><b>Popis</b></th>
			<th>Info</th>
		</tr>
		<?php
			$i=1;
			foreach($parts as $part){
				echo "<form action='' method='post' class='stockCSS'>";
					if($i%2)
						echo "<tr style='background-color:#eeeeee'>";
					else
						echo "<tr>";
					
					echo "<td>".$part->type."</td>";
					echo "<td><b>".$part->partname."</b></td>";
					echo "<td>".$part->description."</td>";
					
					echo "<td align='center'>";

					if($part->active)
						echo "<button type='submit' name='editPart' value='".$part->id."'><span class='dashicons dashicons-info'></span></button> ";
					else
						echo "<button type='submit' name='editPart' value='".$part->id."'><span class='dashicons dashicons-info' style='color: red;'></span></button> ";
					
					if($part->pdf == "")
						echo "<button type='submit' name='addPDF' value='".$part->id."'><span class='dashicons dashicons-pdf'></span></button> ";
					else
						echo "<button type='submit' name='addPDF' value='".$part->id."'><span class='dashicons dashicons-pdf' style='color: #00ff00;'></span></button> ";

					if($part->image == "")
						echo "<button type='submit' name='addIMG' value='".$part->id."'><span class='dashicons dashicons-format-image'></span></button> ";
					else
						echo "<button type='submit' name='addIMG' value='".$part->id."'><span class='dashicons dashicons-format-image' style='color: #00ff00;'></span></button> ";

					if($part->multipart)
						echo "<button type='submit' name='addMultiParts' value='".$part->id."'><span class='dashicons dashicons-editor-ul' style='color: #00ff00;'></span></button> ";
					else
						echo "<button type='submit' name='addMultiParts' value='".$part->id."'><span class='dashicons dashicons-editor-ul' ></span></button> ";
						
						$current_user = wp_get_current_user();
						$is_admin = get_user_meta( $current_user->ID, 'warehouse_is_admin', true);
						if($is_admin == '1')
							echo "<button type='submit' name='changeQauntity' value='".$part->id."'><span class='dashicons dashicons-database-view'></span></button> ";
					echo "</td>";
					echo "</tr>";
				echo "</form>";
				++$i;
			}
		?>
		</table>
		<?php }
	else if(isset($_POST['addMultiParts']) || isset($_SESSION['createMultipartID'])){ 
		include_once  WAREHOUSE_DIR_SERVER."inc/createMultipart.php";

		if(!isset($_SESSION['createMultipartID'])){
			$_SESSION['createMultipartID'] = $_POST['addMultiParts'];
		}
		warehouse_create_multipart($_SESSION['createMultipartID']);

	?>
	
	<?php }
	else if(isset($_SESSION['editedPart'])){ ?>
	<form action="" method="post" class='stockCSS'>
		<div class="row">
			<div class="two_columns">
				<div>
					<label for="pname" id="name_label">Název součástky: </label>
					<label for="pname" id="name_label" class='subLabel'><?php echo strtoupper($editedPart->partname) ?></label>
				</div>
				
				
			</div>
			<div class="two_columns">
				<div>
					<label for="bar_code">Čárový kód:</label>
					<label for="bar_code" class='subLabel'><?php echo strtoupper($editedPart->barcode) ?></label>
				</div>
			</div>
		</div> 

		<div class="row">
			<div class="one_column">
				<label for="human_readable">Lidsky čitelný název:</label>
				<input id="human_readable" type="text" name="human_readable" value="<?php if(isset($_POST['human_readable'])) echo $_POST['human_readable']; else echo $editedPart->value ?>" />
			</div>
		</div>

		<div class="row">
			<div class="one_column">
				<label for="short_desc">Krátký popis: <font color="red">*</font></label>
				<input id="short_desc" type="text" name="short_desc" value="<?php if(isset($_POST['short_desc'])) echo $_POST['short_desc']; else echo $editedPart->description ?>"  required/>
			</div>
		</div>

		<div class="row">
			<div class="two_columns">
				<div>
					<label for="manufacturer" id="name_label">Výrobce: <font color="red">*</font></label>
					<input id="manufacturer" type="text" name="manufacturer" value="<?php if(isset($_POST['manufacturer'])) echo $_POST['manufacturer']; else echo $editedPart->manufacturer ?>" required />
				</div>
				
				
			</div>
			<div class="two_columns">
				<div>
					<label for="supplier">Dodavatel:</label>
					<input id="supplier" type="text" name="supplier" value="<?php if(isset($_POST['supplier'])) echo $_POST['supplier']; else echo $editedPart->currentSupplier ?>" />
				</div>
			</div>
		</div> 
		
		<div class="row">
			<div class="four_columns">
				<div>
					<label for="type" id="type_label">Typ:</label>
					<select id="type" type="text" name="type"/>
					<option value=''></option>
					<?php
						$storedPartTypes = json_decode(getSettingsValue("PART_TYPES_DEF"), true);
						if($storedPartTypes == "")
							$storedPartTypes = array(); 
						foreach($storedPartTypes as $type){
							$selected = "";
							if(isset($_POST['type'])) {
								if($_POST['type'] == $type['type']) 
									$selected = "selected";
							}
							else {
								if($editedPart->type == $type['type']) 
									$selected = "selected";
							}

							echo "<option value='".$type['type']."' ".$selected.">".$type['type']."</option>";
						}
					?>
					</select>
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="percentLoss">Chybovost (%):</label>
					<input id="percentLoss" type="text" name="percentLoss" value="<?php if(isset($_POST['percentLoss'])) echo $_POST['percentLoss']; else echo $editedPart->percentLoss ?>"/>
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="position">Pozice ve skladu:</label>
					<input id="position" type="text" name="position" value="<?php if(isset($_POST['position'])) echo $_POST['position']; else echo $editedPart->position ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="posColour">Barva pozice ve skladu:</label> 							
					<select name='posColour' id='posColour' value="<?php if(isset($_POST['posColour'])) echo $_POST['posColour']; else echo $editedPart->posColour; ?>" >
						<option <?php if ($editedPart->posColour == "#000000" ) echo 'selected' ; ?> value="#000000" style="background: #000000;">Černá</option>
						<option <?php if ($editedPart->posColour == "#d24dff" ) echo 'selected' ; ?> value="#d24dff" style="background: #d24dff;">Fialová</option>
						<option <?php if ($editedPart->posColour == "#6699ff" ) echo 'selected' ; ?> value="#6699ff" style="background: #6699ff;">Modrá</option>
						<option <?php if ($editedPart->posColour == "#33cc33" ) echo 'selected' ; ?> value="#33cc33" style="background: #33cc33;">Zelená</option>
						<option <?php if ($editedPart->posColour == "#e6e600" ) echo 'selected' ; ?> value="#e6e600" style="background: #e6e600;">Žlutá</option>
						<option <?php if ($editedPart->posColour == "#e68a00" ) echo 'selected' ; ?> value="#e68a00" style="background: #e68a00;">Oranžová</option>
						<option <?php if ($editedPart->posColour == "#ff2222" ) echo 'selected' ; ?> value="#ff2222" style="background: #ff2222;">Červená</option>
					</select>
				</div>
			</div>
		</div> 
		
		<div class="row">
			<div class="four_columns">
				<div>
					<label for="quantityPerBox" id="quantityPerBox_label">Množství v balení:</label>
					<input id="quantityPerBox" type="number" name="quantityPerBox" min="0" value="<?php if(isset($_POST['quantityPerBox'])) echo $_POST['quantityPerBox']; else echo $editedPart->quantityPerBox; ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="boxPackageType" id="boxPackageType_label">Typ balení:</label>
					<input id="boxPackageType" type="text" name="boxPackageType" min="0" value="<?php if(isset($_POST['boxPackageType'])) echo $_POST['boxPackageType']; else echo $editedPart->boxPackageType; ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="min_qty" id="name_label">Minimální množství:</label>
					<input id="min_qty" type="number" name="min_qty" min="0" value="<?php if(isset($_POST['min_qty'])) echo $_POST['min_qty']; else echo $editedPart->minQuantity; ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="lead_time">Doba dodání (dní):</label>
					<input id="lead_time" type="number" min="0" name="lead_time" value="<?php if(isset($_POST['lead_time'])) echo $_POST['lead_time']; else echo $editedPart->leadTime; ?>" />
				</div>
			</div>
		</div>

		<div class="row">
			<div class="four_columns">
				<div>
					<label for="price" id="price_label">Cena:</label>
					<input id="price" type="number" min="0" step="0.0001" name="price" value="<?php if(isset($_POST['price'])) echo $_POST['price']; else echo $editedPart->price; ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="currency" id="currency_label">Měna:</label>
					<input id="currency" type="text" name="currency" value="<?php if(isset($_POST['currency'])) echo $_POST['currency']; else echo $editedPart->currency; ?>" />
				</div>
			</div>
			
			<div class="two_columns">
				<label for="package" id="name_label">Pouzdro:</label>
				<input id="package" type="text" name="package" value="<?php if(isset($_POST['package'])) echo $_POST['package']; else echo $editedPart->package ?>" />
			</div>
		</div> 

		<div class="row">
			<div class="two_columns">
				<label for="replacement">Náhrada:</label>
				<input id="replacement" type="text" name="replacement" value="<?php if(isset($_POST['replacement'])) echo $_POST['replacement']; else echo $editedPart->replacement ?>"/>
			</div>

			<div class="two_columns">
				<label for="sku">SKU:</label>
				<input id="sku" type="text" name="sku" value="<?php if(isset($_POST['sku'])) echo $_POST['sku']; else echo $editedPart->sku ?>"/>
			</div>
		</div>
		
		<div class="row">
			<div class="one_column">
				<label for="details">Detailní popis:</label>
				<textarea id="details" rows="4" cols="50" name="details"><?php if(isset($_POST['details'])) echo $_POST['details']; else echo $editedPart->details ?></textarea>
			</div>
		</div>
		
		<div class="row">
			<div class="one_column">
				<label for="activePart">Aktivní položka:</label>
				<label class='switch'><input id="activePart" type="checkbox" name="activePart" <?php echo $activePart ?> > <span class='slider round'></span></label>
			</div>
		</div>
		
		<div class="row">
			<?php
			$current_user = wp_get_current_user();
			$is_admin = get_user_meta( $current_user->ID, 'warehouse_is_admin', true);
			if($is_admin == '1'){
			?>
				<!--<div class="two_columns">
					<button name="deletePart" type="submit" style="float: left;" value="<?php echo $_POST['editPart'] ?>">Smazat</button>
				</div>
				-->
				<div class="two_columns">
					<button name="savePart" type="submit" style="float: right;" value="<?php echo $_SESSION['editedPart'] ?>" autofocus>Uložit</button>
				</div>
			<?php
			}
			else {
			?>
				<div class="one_column">
					<button name="savePart" type="submit" style="float: right;" value="<?php echo $_SESSION['editedPart'] ?>" autofocus>Uložit</button>
				</div>
			<?php
			}
			?>
		</div>
	</form>

	<?php
		if($editedPart->multipart){
			showMultiParts(array( 0 => $listOfParts), 0, 0);
		}

	}
	else if(isset($_SESSION['changeQauntity'])){ ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">

	<script>
	$("input[type='text']").on("focus", function() {
            $(this).select();
        });
	</script>
		<form action="" method="post" class='stockCSS'>
			<div class="row">
				<div class="two_columns">
					<div>
						<label for="pname" id="name_label">Název součástky: </label>
						<label for="pname" id="name_label" class='subLabel'><?php echo strtoupper($editedPart->partname) ?></label>
					</div>					
				</div>
				<div class="two_columns">
					<div>
						<label for="bar_code">Čárový kód:</label>
						<label for="bar_code" id="bar_code" class='subLabel'><?php echo strtoupper($editedPart->barcode) ?></label>
					</div>
				</div>
			</div> 
			
			<div class="row">
				<div class="one_column">
					<label for="short_desc">Krátký popis: </label>
					<label for="short_desc"  id="short_desc" class='subLabel'> <?php echo $editedPart->description ?> </label>
				</div>
			</div>
	
			<div class="row">
				<div class="four_columns">
					<div>
						<label for="quantity" id="quantity_label">Množství:</label>
						<input id="quantity" type="number" min="0" step="1" name="quantity" autofocus value="<?php echo $editedPart->quantity; ?>" />
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="two_columns">
					<button name="saveChQTY" type="submit" style="float: right;" value="<?php echo $_POST['changeQauntity'] ?>" autofocus>Uložit</button>
				</div>
				<div class="two_columns">
					<button name="btnBackChQTY" type="submit" style="float: left;">Zpět</button>
				</div>

			</div>
		</form>
	<?php 
	}
	else if(isset($_POST['addPDF']) || isset($_POST['addIMG']) || isset($_POST['addROHS'])){
		?>
		<form action="" method="post"  enctype="multipart/form-data" class='stockCSS'>
			<div class="row">
				<div class="two_columns">
					<div>
						<label for="pname" id="name_label">Název součástky: </label>
						<label for="pname" id="name_label" class='subLabel'><?php echo strtoupper($editedPart->partname); ?></label>
					</div>
					
					
				</div>
				<div class="two_columns">
					<div>
						<label for="bar_code">Čárový kód:</label>
						<label for="bar_code" class='subLabel'><?php echo strtoupper($editedPart->barcode); ?></label>
					</div>
				</div>
			</div> 
			
			<div class="row">
				<div class="one_column">
					<div>
						<label for="short_desc">Krátký popis: </label>
						<label for="short_desc" class='subLabel'><?php echo $editedPart->description; ?> </label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="one_column"> <?php
					if(isset($_POST['addPDF'])){ ?>
					Select a PDF file to upload:
					<div>
						<input type="file" name="fileToUpload" id="fileToUpload" accept="application/pdf" >
					</div>		
					<?php }
					
					if(isset($_POST['addIMG'])){ ?>
					
					Select a JPG file to upload:
					<div>
						<input type="file" name="fileToUpload" id="fileToUpload" accept="image/jpeg" >
					</div>
					<?php 
					} 

					if(isset($_POST['addROHS'])){ ?>
					Select a PDF file with ROHS certificate to upload:
					<div>
						<input type="file" name="fileToUpload" id="fileToUpload" accept="application/pdf" >
					</div>
					<?php 
					} ?>
				</div>
			</div> 

			<br>
			
			<div class="progress">
				<div class="bar"></div >
				<div class="percent">0%</div >
			</div>

			<br>

			<div class="row">
				<div class="one_column">
					<button type="submit" value="<?php 
						if(isset($_POST["addPDF"])) echo $_POST["addPDF"]; 
						if(isset($_POST["addIMG"])) echo $_POST["addIMG"];
						if(isset($_POST["addROHS"])) echo $_POST["addROHS"];
						?>" 
						name="uploadToServer" > Upload</button>
				</div>
			</div>
		</form>
	<?php 
	}
	?>
</div><!-- .wrap -->

<?php
}
add_shortcode('warehouse_edit_components', 'warehouse_edit_components'); 