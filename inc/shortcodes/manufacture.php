<?php

/**
 * Template Name: Page related to manufacture components/parts/device
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}
include_once  WAREHOUSE_DIR_SERVER."inc/common/functions.php";
include_once  WAREHOUSE_DIR_SERVER."inc/common/definitions.php";

$requiredComponents = [];

function addMultiPartManufacture($partID, $quantity, $level=0, $purpose = array('name'=>'', 'qty'=>0, 'singleQty'=>0, 'designator' => "")){
	global $wpdb;
    global $requiredComponents;

	$purpose['singleQty'] = $quantity;

	// get current quantity of the goods
	$table_name = $wpdb->prefix.'warehouse_parts';
	$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$partID);
	if(empty($results)){
		return NULL;
	}
	$addedPartTop = $results[0];

    $x = array_search($addedPartTop->id, array_column($requiredComponents, 'id'));
    if($x == false){
        $table_name = $wpdb->prefix.'warehouse_parts';
        $results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$addedPartTop->id);
        $quantityPart = $results[0]->quantity;
        $new = array( 	'id' 				=> $results[0]->id, 
                        'quantity'  		=>  -$quantityPart,
                        'multipart' 		=> $results[0]->multipart,
                        'partname'  		=> $results[0]->partname,
                        'type'      		=> $results[0]->type,
						'package'      		=> $results[0]->package,
						'purpose'			=> []);
        array_push($requiredComponents, $new);
    }
	$id_manufacture = array_search($addedPartTop->id, array_column($requiredComponents, 'id'));   
    $requiredComponents[$id_manufacture]['quantity'] += $quantity;

    if($requiredComponents[$id_manufacture]['quantity'] < 0)
        return;

	//print_r(array_column($requiredComponents[$id_manufacture]['purpose'], 'name'));
	//print_r($purpose['name']);
	$purposeFound = array_search($purpose['name'], array_column($requiredComponents[$id_manufacture]['purpose'], 'name'));
	//var_dump($purposeFound);
	//print_r("<br>");
	if(!is_numeric($purposeFound)){
		array_push($requiredComponents[$id_manufacture]['purpose'], $purpose);
	}
    
    $quantity = $requiredComponents[$id_manufacture]['quantity'];

	if($addedPartTop->multipart && $level <= 10){
		// read list of parts
		$table_name = $wpdb->prefix.'warehouse_multipart';
		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id_parent`=".$addedPartTop->id);
		$parts = $results;

		foreach($parts as $part){
			$purpose['designator'] = $part->designator;
            addMultiPartManufacture($part->id_part, $part->quantity * $quantity, $level+1, $purpose);
		}
	}
}

function warehouse_manufacture($atts) { 
	global $wpdb;
    global $requiredComponents;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );

	$current_user = wp_get_current_user();
	
	if(!isset($_SESSION['manufactureStep'])){
		$_SESSION['manufactureStep'] = 1;
	}
	
	if(isset($_POST['btnCheck']) | isset($_SESSION['manufacture_scannedParts'])){		
		if(isset($_POST['btnCheck']))
			$_SESSION['manufacture_scannedParts'] = $_POST['scannedParts'];
		
		$decodedParts = array();
		$allValid = true;
		$receiptNumber = intval(getSettingsValue("EXPORT_RECEIPT_NUMBER"));
		$receiptTextNumber = str_pad($receiptNumber, 8, '0', STR_PAD_LEFT);

		foreach(preg_split("/((\r?\n)|(\r\n?))/", $_SESSION['manufacture_scannedParts']) as $line){
			if($line == "")
				continue;
			
			$fields = explode (";", $line); 
			
			if(count($fields) >= 2){
				// get current quantity of the goods
				$table_name = $wpdb->prefix.'warehouse_parts';
				$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `partname`='".trim($fields[0])."'");
				
				if(!empty($results)){
					$part = $results[0];
					
					if(count($fields) == 2){
						$x = array_search($part->partname, array_column($decodedParts, 'name'));
						if($x !== false){
							$decodedParts[$x]['quantity'] += $fields[1];
						}
						else
							array_push($decodedParts, array('id'			=> $part->id, 
                                                            'name'          => $part->partname,
                                                            'stockQuantity' => $part->quantity,
															'quantity' 		=> intval($fields[1]), 
                                                            'description'	=> $part->description, 
                                                            'price' 		=> $part->price, 
                                                            'currency'		=> $part->currency, 
															'package'      	=> $part->package,
															'valid' 		=> 1
							));
					}
				}	
			}
		} 
		
		$_SESSION['manufactureStep'] = 2;
		$_SESSION['manufacture_decodedParts'] = $decodedParts;
		$_SESSION['manufacture_noIDs'] = count($decodedParts);
	}
	
	if(isset($_POST['btnSave'])){
		$parts = $_SESSION['manufacture_decodedParts'];
        // assign order number to log the order
        $receiptNumber = intval(getSettingsValue("manufacture_NUMBER"));
        $receiptTextNumber = str_pad($receiptNumber, 8, '0', STR_PAD_LEFT);

		for($i=1; $i<=$_SESSION['manufacture_noIDs']; ++$i){
			$text = 'quantity'.$i;
			$quantity = intval($_POST[$text]);
			
            $text = 'id'.$i;
			$partID = $_POST[$text];
			
			// get current quantity of the goods
			$table_name = $wpdb->prefix.'warehouse_manufacture_plan';
            $data  = array( 'id_part'				=> $partID, 
                            'quantity'				=> $quantity, 
                            'id_user'		        => $current_user->ID,
							'state'					=> constant("MANUFACTURE_ON_PERPARATION")
            );
            $wpdb->insert($table_name, $data);					
		}

		$_SESSION['manufactureStep'] = 3;
		unset($_SESSION['manufacture_decodedParts']);
		unset($_SESSION['manufacture_noIDs']);
		unset($_SESSION['manufacture_scannedParts']);
	}
	
	if(isset($_POST['btnBack'])){
		--$_SESSION['manufactureStep'];
	}
	
?>


<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<div class="wrap">
		<?php 
        if(isset($_GET['newRecord'])){
			switch($_SESSION['manufactureStep']){
				case 1:
			?>
				
				<div align='center'>
					<button class="tablinkSel" id="scan" style="float: center;">Skenovat</button>
					<button class="tablink" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				
				<div>
					<form action="" method="post"  class="stockCSS">
						<div class="one_column">
							<label for="scannedParts" id="name_label">Naskenované součástky a jejich množství: <font color="red">*</font></label>
							<textarea id="scannedParts" rows="15" cols="50" name="scannedParts" required autofocus><?php if(isset($_SESSION['manufacture_scannedParts'])) echo $_SESSION['manufacture_scannedParts'] ?></textarea>
						</div>
						<div>
							<b>Format:</b> Barcode;Quantity
						</div>
						<div class="one_column">
							<button name="btnCheck" type="submit" style="float: right;">Zkontrolovat</button>
						</div>
					</form>
				</div>
			<?php
				break;
				case 2:
			?>
				<div align='center'>
					<button class="tablink" id="scan" style="float: center;">Skenovat</button>
					<button class="tablinkSel" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				
				<form action="" method="post"  class="stockCSS">
					<div class="row">
						<div class="two_columns">
							<div>
								<label>Uživatel:</label>
								<label class='subLabel'><?php echo $current_user->display_name ?></label>
							</div>
						</div>
						<div class="two_columns">
							<div>
							    <label>Účel: <font color="red">*</font></label>
								<input id="purpose" type="text" name="purpose" value="<?php if(isset($_POST['purpose'])) echo $_POST['purpose']; ?>" required autofocus />
							</div>
						</div>
					</div> 
					
					<table class="stockCSS">
					<tr class='headerRow'>
						<th>Název</th>
						<th class='thCenter'>Na skladě<br/>Na objednávce</th>
						<th>Kupované množství</th>
						<th>Cena</th>
						<th>Popis</th>
					</tr>
					<?php
						$id = 0;
						foreach($decodedParts as $part){
							++$id;									
								if($part['valid'] == 0){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"2\">".$part['name']."</td>";
									echo "<td>".$part['quantity']."</td>";
									echo "<td>".$part['description']."</td>";
								}
								else if($part['valid'] == -1){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"6\">".$part['text']."</td>";
								}
								else{
									echo "<tr>";
									echo "<td>".$part['name']."<input style='display:none;' type='number' name='id".$id."' value='".intval($part['id'])."'\"></td>";
									echo "<td class='tdCenter'>".$part['stockQuantity']."</td>";
									echo "<td><input type='number' min='0' "."' name='quantity".$id."'  value='".intval($part['quantity'])."'></td>";
                                    echo "<td>".$part['price']." ".$part['currency']."</td>";
									echo "<td>".$part['description']."</td>";
								}
								echo "</tr>";
							echo "</form>";
						}
					?>
					
					</table>

					<div class="one_column">
						<script>
							function removeRequired(form){
								$.each(form, function(key, value) {
									if ( value.hasAttribute("required")){
										value.removeAttribute("required");
									}
								});
							}
						</script>
						<?php if($allValid == true)
							echo "<button name='btnSave' type='submit' style='float: right;'>Do košíku</button>";
						?>	
						<button name="btnBack" type="submit" style="float: left;" onClick="removeRequired(this.form)">Zpět</button>
					</div>
				</form>
			
			<?php
				break;
				case 3:
			?>
				<div align='center'>
					<button class="tablink" id="scan" style="float: center;">Skenovat</button>
					<button class="tablink" id="check" style="float: center;">Kontrola</button>
					<button class="tablinkSel" id="save" style="float: center;">Uložit</button>
				</div>
				
				<h2 class="stockCSS"><font color="green">Záznam uložen</font></h2>
				
				<form action="" method="post" class="stockCSS">
					<div class="one_column">
						<button name="btnNew" type="submit" style="float: right;">Nový záznam</button>
					</div>
				</form>
			<?php
				unset($_SESSION['manufactureStep']);
				break;
			}
			?>

    </div><!-- .wrap -->

    <?php
        } // end of if(isset($_GET['newRecord']))
        else{
            // read list of parts
            $table_name = $wpdb->prefix.'warehouse_manufacture_plan';
            $results = $wpdb->get_results("SELECT * FROM $table_name WHERE `state`=0");
            $parts = $results;
            
            $requiredComponents = [];
            foreach($parts as $partMFG){
				$table_name = $wpdb->prefix.'warehouse_parts';
				$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$partMFG->id_part);
				$part = $results[0];
                addMultiPartManufacture($partMFG->id_part, $partMFG->quantity, 0, array('name'=>$part->partname, 'qty'=>$partMFG->quantity, 'singleQty'=>$partMFG->quantity, 'designator' => $part->designator));
            }

            $type  = array_column($requiredComponents, 'type');
            $partname = array_map('strtolower', array_column($requiredComponents, 'partname'));
            array_multisort($type, SORT_ASC, $partname, SORT_ASC, $requiredComponents);	

			?>
			<form action="" method="get"  class="stockCSS">
				<div class="row">
					<div class="two_columns">
						<div class='warehouseItem'>
							<input id="qtyPerPkg" type="checkbox" name="qtyPerPkg" <?php if (isset($_GET['qtyPerPkg'])) echo 'checked'; ?> />
							<label for="qtyPerPkg">Nakup po celem baleni</label>
						</div>

						<div class='warehouseItem'>
							<input id="orgQTY" type="checkbox" name="orgQTY" <?php if (isset($_GET['orgQTY'])) echo 'checked'; ?> >
							<label for="orgQTY">Zobrazit puvodni mnozstvi</label>
						</div>

						<div class='warehouseItem'>
							<input id="hidePurpose" type="checkbox" name="hidePurpose" <?php if (isset($_GET['hidePurpose'])) echo 'checked'; ?> >
							<label for="hidePurpose">Skryt ucel</label>
						</div>

						<div class='warehouseItem'>
							<input id="hideStockQTY" type="checkbox" name="hideStockQTY" <?php if (isset($_GET['hideStockQTY'])) echo 'checked'; ?> >
							<label for="hideStockQTY">Skryt Pocet ve skladu</label>
						</div>

						<div class='warehouseItem'>
							<input id="showDesignator" type="checkbox" name="showDesignator" <?php if (isset($_GET['showDesignator'])) echo 'checked'; ?> >
							<label for="showDesignator">Zobrazit Designator</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="four_columns">
						<button type="submit" style="float: left;">Změnit</button>
					</div>
				</div>

			</form>
			<?php
			echo "<table style='margin:5%;width:90%'>";
			echo 	"<tr>";
			echo        "<th>IMG</th>";
			echo 		"<th>SKU</th>";
			echo 		"<th>Partname <span style='float: right;'>Info</span></th>";
            echo 		"<th>Type</th>";
            echo 		"<th>Package</th>";
			echo 		"<th>Description</th>";
			
			if(!isset($_GET['hideStockQTY']))
			echo 		"<th>In Stock</th>";
			echo 		"<th>QTY</th>";
			echo 		"<th>QTY with backup</th>";
			echo 		"<th>QTY per pkg</th>";
			echo 		"<th colspan='2'>Unit price</th>";
			echo 		"<th colspan='2'>Total price</th>";
			if(!isset($_GET['hidePurpose']))
			echo 		"<th>Target device/s</th>";
			echo 	"</tr>";

            //print_r($requiredComponents);
            $warehouseUrl = get_permalink( get_page_by_path( 'warehouse' ));
            $totalPrice = [];
            foreach ($requiredComponents as $key => $value) {
                if($value['quantity'] <= 0)
                    continue;

                    //echo $key . ' -> ' . $value['quantity'] . '<br>';
                    $table_name = $wpdb->prefix.'warehouse_parts';
	                $results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$value['id']);
                    $part = $results[0];
                    
                    echo "<tr>";
                    echo "<td>";
					if($part->image != "")
						echo '<img src="'.$part->image.'" style="height:100px;width:auto;"> ';
				
					echo "</td>";
                    echo 	"<td align='center'>".$part->sku."</td>";
                    echo 	"<td><b><a href='".$warehouseUrl."?detailID=".$part->id."'>".$part->partname."</a>"."</b>";

                    echo    "<span style='float: right;'>";
                    if($part->pdf != "")
                        echo " <a class='dashicons dashicons-pdf' href='".$part->pdf."'/> ";
                    
                    if($part->image != "")
                        echo " <a class='dashicons dashicons-format-image' href='".$part->image."'/> ";

                    if($part->rohs != "")
                        echo " <a class='dashicons dashicons-format-aside' style='color: #2AA243;' href='".$part->rohs."'/> ";

                    if($part->multipart){
                        $url = get_permalink( get_page_by_path( 'warehouse/list_of_multiparts' ));
                        echo " <a class='dashicons dashicons-editor-ul' href='".$url."?part_id=".$part->id."'/> ";
                    }
                    echo    "</span>";
                    echo 	"</td>";


                    echo 	"<td align='center'>".$part->type."</td>";
					echo 	"<td align='center'>".$part->package."</td>";
                    echo    "<td>";
                    echo 	" ".$part->description;
                    echo 	"</td>";
					if(!isset($_GET['hideStockQTY']))
                    	echo 	"<td align='center'>".$part->quantity."</td>";
					if(isset($_GET['qtyPerPkg']) && $part->quantityPerBox > 1){
						$orgQTY = $value['quantity'];
						$value['quantity'] = roundUpStep($value['quantity'],$part->quantityPerBox);
					}
					if(isset($orgQTY) && isset($_GET['orgQTY'])){
						echo 	"<td align='center'>".$value['quantity']."<br><font color='#c7c7c7'>(".$orgQTY.")</font></td>";
					}
					else{
                    	echo 	"<td align='center'>".$value['quantity']."</td>";
					}

					$table_name = $wpdb->prefix.'warehouse_backup';
					$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `package`='".$value['package']."'");
					$value['quantityWithBackup'] = $value['quantity'];
					if(count($results) > 0){
						$parts = $results;
						if($value['quantity'] <= 10)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty10;
						else if($value['quantity'] <= 25)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty25;
						else if($value['quantity'] <= 50)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty50;
						else if($value['quantity'] <= 100)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty100;
						else if($value['quantity'] <= 200)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty200;
						else if($value['quantity'] <= 500)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty500;
						else if($value['quantity'] <= 1000)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty1000;
						else if($value['quantity'] <= 2000)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty2000;
						else if($value['quantity'] <= 5000)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty5000;
						else if($value['quantity'] <= 10000)
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qty10000;
						else 
							$value['quantityWithBackup'] = $value['quantity'] + $results[0]->qtyAbove;

							echo 	"<td align='center'>".$value['quantityWithBackup']."</td>";
					}
					else{
						echo 	"<td align='center'>-</td>";
					}

					
					

					if($part->quantityPerBox == 0)
						echo 	"<td align='center'>-</td>";
					else
                    	echo 	"<td align='center'>".$part->quantityPerBox ."</td>";
                    
                    $partPrice = calculateTotalPartPrice($part->id) * $value['quantityWithBackup'];
					if(isset($orgQTY) && isset($_GET['orgQTY'])){
						$partPriceOrgQTY = calculateTotalPartPrice($part->id) * $orgQTY;
					}

					if($part->multipart)
                        echo 	"<td align='center'> - </td><td> - </td>";
                    else   {
                        echo 	"<td align='right'>".round(calculateTotalPartPrice($part->id),3);
						echo "</td><td> ".$part->currency."</td>";
                    }

                    if($part->multipart)
                        echo 	"<td align='center'> - </td><td> - </td>";
                    else   {
                        echo 	"<td align='right'>".round($partPrice,3);
						if(isset($orgQTY) && isset($_GET['orgQTY'])){
							echo "<br><font color='#c7c7c7'>(".round($partPriceOrgQTY,3).")</font>";
						}
						echo "</td><td> ".$part->currency."</td>";
                        if(!isset($totalPrice[$part->currency])){
                            $totalPrice[$part->currency] = 0;
                            $totalPrice[$part->currency] = $partPrice;
                        }
                        else
                            $totalPrice[$part->currency] += $partPrice;
                    }

					if(!isset($_GET['hidePurpose'])){
						echo 	"<td>";					
						foreach($value['purpose'] as $target){
							echo "<b>".$target['singleQty']."x </b> for ".$target['name']."</br>";
							if(isset($_GET['showDesignator'])){
								echo "(".$target['designator'].")</br>";
							}
						}
						echo	"</td>";
					}
                    echo "</tr>";
					unset($orgQTY);
            }
            $sumHomeCurrency = 0;
            $homeCurrency = getSettingsValue("CURRENCY_HOME");

            foreach($totalPrice as $curr => $price){
				echo "<tr><td><b>Price in ".$curr."</td><td colspan='8'>" ;
				if($curr == $homeCurrency){
					$sumHomeCurrency += $price;
				}
				else{
					$currRatio = getSettingsValue("CONV;".$curr.";".$homeCurrency);
					$sumHomeCurrency += $price * floatval($currRatio);
					if($currRatio > 0)
						$invRatio = 1/$currRatio;
					else
						$invRatio = -1;
					echo "Conversion rate 1 ".$curr." = ".$currRatio." ".$homeCurrency." | 1 ".$homeCurrency." = ".round($invRatio, 3)." ".$curr;
				}
				echo "</td><td align='right' colspan='2'><b>".round($price,3)." ".$curr."</b></td></tr>";
			}
		
			echo "<tr><td><b><font color='red'>Total price in ".$homeCurrency."</font></b></td><td colspan='8'></td><td align='right'  colspan='2'><font color='red'><b>".round($sumHomeCurrency,3)." ".$homeCurrency."</b></font></td></tr>";
			echo "</table>";
        }
}

add_shortcode('warehouse_manufacture', 'warehouse_manufacture');


?>
