<?php

/**
 * Template Name: New part
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function warehouse_new_component($atts) { 
	global $wpdb;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );
	
	$message = "";
	$errorSave = false;

	if(isset($_POST['newPart'])){
		$errorSave = false;

		$table_name = $wpdb->prefix.'warehouse_parts';
		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `partname`='".$_POST['partname']."'");
		if(!empty($results)){
			$message = $message."Položka '".strtoupper($_POST['partname'])."' již v seznamu existuje<br>";
			$errorSave = true;
		}

		$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `barcode`='".$_POST['barcode']."'");
		if(!empty($results)){
			$message = $message."Čárový kód '".strtoupper($_POST['barcode'])."' již v seznamu existuje<br>";
			$errorSave = true;
		}

		if($_POST['type'] == ""){
			$message = $message."You have to specify part type <br/>";
			$errorSave = true;
		}

		if($_POST['sku'] != ""){
			$table_name = $wpdb->prefix.'warehouse_parts';
			$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `sku`='".$_POST['sku']."'");
			
			if(!empty($results)){			
				$message = $message."SKU '".strtoupper($_POST['sku'])."' již v seznamu existuje u součástky s partname: ".$results[0]->partname."<br>";
				$errorSave = true;
			}
		}

		if($errorSave == false){
			$data = 	array(	
							'partname'			=> 	strtoupper($_POST['partname']),
							'barcode'			=> 	strtoupper($_POST['barcode']),
							'manufacturer'		=>	$_POST['manufacturer'],
							'package'			=>	$_POST['package'],
							'type'				=>	$_POST['type'],
							'details'			=>	$_POST['details'],
							'description'		=>	$_POST['short_desc'],
							'minQuantity'		=>	$_POST['min_qty'],
							'quantityPerBox'	=>	$_POST['quantityPerBox'],
							'boxPackageType'	=>	$_POST['boxPackageType'],
							'position'			=>	$_POST['position'],
							'posColour'			=>	$_POST['posColour'],
							'currentSupplier'	=>	$_POST['supplier'],
							'leadTime'			=>	$_POST['lead_time'],
							'replacement'		=>	$_POST['replacement'],
							'price'				=>	$_POST['price'],
							'currency'			=>	$_POST['currency'],
							'value'				=>  $_POST['human_readable'],
							'percentLoss'		=>  $_POST['percentLoss'],
							'sku'				=>  $_POST['sku'],
							'active'			=>  1

			);
			$wpdb->insert($table_name, $data);
			$message = "Položka ".strtoupper($_POST['partname'])." uložena";
		}		
	}
?>

<div class="one_column">	
	<?php if($errorSave == true)
		echo "<font color='red'>".$message."</font>";
	else
		echo "<font color='green'>".$message."</font>";
	?>

	<form action="" method="post"  class="stockCSS">
		<div class="row">
			<div class="two_columns">
				<div>
					<div id="qr-reader" style="width:500px"></div>
					<label for="partname" id="name_label">Název součástky výrobce: <font color="red">*</font></label>
					<input id="partname" type="text" name="partname" value="<?php if (isset($_POST['partname'])) echo strtoupper($_POST['partname']) ?>" required/>
				</div>
				
				
			</div>
			<div class="two_columns">
				<div>
					<label for="barcode">Čárový kód: <font color="red">*</font></label>
					<input id="barcode" type="text" name="barcode" value="<?php if (isset($_POST['barcode']))  echo strtoupper($_POST['barcode']) ?>" required/>
				</div>
			</div>
		</div> 

		<div class="row">
			<div class="one_column">
				<label for="short_desc">Krátký popis: <font color="red">*</font></label>
				<input id="short_desc" type="text" name="short_desc" value="<?php if (isset($_POST['short_desc']))  echo $_POST['short_desc'] ?>"  required/>
			</div>
		</div>

		<div class="row">
			<div class="one_column">
				<label for="human_readable">Lidsky čitelný název:</label>
				<input id="human_readable" type="text" name="human_readable" value="<?php if (isset($_POST['human_readable']))  echo $_POST['human_readable'] ?>" />
			</div>
		</div>
		
		<div class="row">
			<div class="two_columns">
				<div>
					<label for="manufacturer" id="name_label">Výrobce: <font color="red">*</font></label>
					<input id="manufacturer" type="text" name="manufacturer" value="<?php if (isset($_POST['manufacturer']))  echo $_POST['manufacturer'] ?>"  required/>
				</div>
				
				
			</div>
			<div class="two_columns">
				<div>
					<label for="supplier">Dodavatel:</label>
					<input id="supplier" type="text" name="supplier" value="<?php if (isset($_POST['supplier']))  echo $_POST['supplier'] ?>" />
				</div>
			</div>
		</div> 
		
		<div class="row">
			<div class="four_columns">
				<div>
					<label for="type" id="type_label">Typ:</label>
					<select id="type" type="text" name="type" />
					<option value=''></option>
					<?php
						$storedPartTypes = json_decode(getSettingsValue("PART_TYPES_DEF"), true);
						if($storedPartTypes == "")
							$storedPartTypes = array(); 

						foreach($storedPartTypes as $type){
							$selected = "";
							if(isset($_POST['type'])) {
								if($_POST['type'] == $type['type']) 
									$selected = "selected";
							}

							echo "<option value='".$type['type']."' ".$selected.">".$type['type']."</option>";
						}
					?>
					</select>
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="percentLoss">Chybovost (%):</label>
					<input id="percentLoss" type="text" name="percentLoss" value="<?php if (isset($_POST['percentLoss']))  echo $_POST['percentLoss'] ?>" />
				</div>
				
				
			</div>
			<div class="four_columns">
				<div>
					<label for="position">Pozice ve skladu:</label>
					<input id="position" type="text" name="position" value="<?php if (isset($_POST['position']))  echo $_POST['position'] ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="posColour">Barva pozice ve skladu:</label> 							
					<select name='posColour' id='posColour' value="<?php if (isset($_POST['posColour']))  echo $_POST['posColour']; ?>" >
						<option value="#000000" style="background: #000000;">Černá</option>
						<option value="#d24dff" style="background: #d24dff;">Fialová</option>
						<option value="#6699ff" style="background: #6699ff;">Modrá</option>
						<option value="#33cc33" style="background: #33cc33;">Zelená</option>
						<option value="#e6e600" style="background: #e6e600;">Žlutá</option>
						<option value="#e68a00" style="background: #e68a00;">Oranžová</option>
						<option value="#ff2222" style="background: #ff2222;">Červená</option>
					</select>
				</div>
			</div>
		</div> 
		
		<div class="row">
			<div class="four_columns">
				<div>
					<label for="quantityPerBox" id="quantityPerBox_label">Množství v balení:</label>
					<input id="quantityPerBox" type="number" name="quantityPerBox" min="0" value="<?php if (isset($_POST['quantityPerBox']))  echo $_POST['quantityPerBox'] ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="boxPackageType" id="boxPackageType_label">Typ balení:</label>
					<input id="boxPackageType" type="text" name="boxPackageType" min="0" value="<?php if (isset($_POST['boxPackageType']))  echo $_POST['boxPackageType'] ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="min_qty" id="name_label">Minimální množství:</label>
					<input id="min_qty" type="number" name="min_qty" min="0" value="<?php if (isset($_POST['min_qty']))  echo $_POST['min_qty'] ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="lead_time">Doba dodání (dní):</label>
					<input id="lead_time" type="number" min="0" name="lead_time" value="<?php  if (isset($_POST['lead_time'])) echo $_POST['lead_time'] ?>" />
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="four_columns">
				<div>
					<label for="price" id="price_label">Cena:</label>
					<input id="price" type="number" min="0" step="0.0001" name="price" value="<?php  if (isset($_POST['price'])) echo $_POST['price'] ?>" />
				</div>
			</div>
			<div class="four_columns">
				<div>
					<label for="currency" id="currency_label">Měna:</label>
					<input id="currency" type="text" name="currency" value="<?php  if (isset($_POST['currency'])) echo $_POST['currency'] ?>" />
				</div>
			</div>
			<div class="two_columns">
				<label for="package">Pouzdro:</label>
				<input id="package" type="text" name="package" value="<?php if (isset($_POST['package']))  echo $_POST['package']; ?>"/>
			</div>
		</div> 
		
		<div class="row">
			<div class="two_columns">
				<label for="replacement">Náhrada:</label>
				<input id="replacement" type="text" name="replacement" value="<?php if(isset($_POST['replacement'])) echo $_POST['replacement']; ?>"/>
			</div>

			<div class="two_columns">
				<label for="sku">SKU:</label>
				<input id="sku" type="text" name="sku" value="<?php if(isset($_POST['sku'])) echo $_POST['sku']; ?>"/>
			</div>
		</div>

		<div class="row">
			<div class="one_column">
				<label for="details">Detailní popis:</label>
				<textarea id="details" rows="4" cols="50" name="details"><?php if (isset($_POST['details']))  echo $_POST['details'] ?></textarea>
			</div>
		</div>
		
		<div class="row">
			<div class="one_column">
				<button name="newPart" type="submit" style="float: right;">Uložit</button>
			</div>
		</div>
	</form>
</div>

<?php
}
add_shortcode('warehouse_new_component', 'warehouse_new_component'); 
