<?php

/**
 * Template Name: List of Shortcodes
 * Shortcode                                Meaning
 * --------------------------------------------------------------------------------------------------------------------------
 * [warehouse_list_of_components]           Get list of components listed in database
 * [warehouse_edit_components]              Shortcode for editing parts (includes detailed list, edit and save function)
 * [warehouse_new_component]                Add new component
 * [warehouse_import_goods_to_warehouse]    Import goods to the warehouse
 * [warehouse_export_goods_from_warehouse]  Export/use goods from the warehouse
 * [warehouse_list_of_multiparts]           Show list of part for parent part (assembled from multiple parts)
 * [warehouse_order_components]             Create a component order list 
 * [warehouse_cart]                         Cart with components to order
 * [warehouse_manufacture]                  Page related to manufacture components/parts/device
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}

include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/listOfParts.php";
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/editParts.php";
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/newPart.php";
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/importGoods.php";
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/exportGoods.php";
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/listOfMultiparts.php";
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/orderComponents.php";
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/cart.php";
include_once  WAREHOUSE_DIR_SERVER."inc/shortcodes/manufacture.php";
