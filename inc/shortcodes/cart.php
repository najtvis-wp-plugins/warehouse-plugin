<?php

/**
 * Template Name: Cart with components to order
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// define where is plugin located
if (!defined('WAREHOUSE_DIR_SERVER')) {
    define('WAREHOUSE_DIR_SERVER', plugin_dir_path(__FILE__));
}
include_once  WAREHOUSE_DIR_SERVER."inc/common/functions.php";
include_once  WAREHOUSE_DIR_SERVER."inc/common/definitions.php";

// return number of sessions
function warehouse_cart($atts) { 
	global $wpdb;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );

	?>	
	<?php

	// read list of parts
	$table_name = $wpdb->prefix.'warehouse_order_parts_cart';
	$results = $wpdb->get_results("SELECT * FROM $table_name WHERE `state`=".constant("IN_CART"));
	$parts = $results;
	
    $partsToOrder = [];
    if(isset($_GET['separated'])){
        
        foreach($parts as $part){
            $ID = $part->id_part."_".$part->id;
            $table_name = $wpdb->prefix.'warehouse_parts';
            $results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$part->id_part);

            $partsToOrder[$ID] = array(
                'partname'          => $results[0]->partname,
                'part_id'           => $part->id_part,
                'type'              => $results[0]->type,
                'totalQuantity'     => $part->quantity,
                'purpose'           => $part->purpose, 
                'qtyPurpose'        => array(array(
                    'purpose'   => $part->purpose,  
                    'qty'       => $part->quantity
                    ))
                );
        }
        $purpose  = array_column($partsToOrder, 'purpose');
        $type  = array_column($partsToOrder, 'type');
        $partname = array_map('strtolower', array_column($partsToOrder, 'partname'));
        array_multisort($purpose, SORT_ASC, $type, SORT_ASC, $partname, SORT_ASC, $partsToOrder);
    }
    else{
        foreach($parts as $part){
            if(!isset($partsToOrder[$part->id_part])){
                $table_name = $wpdb->prefix.'warehouse_parts';
                $results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$part->id_part);

                $partsToOrder[$part->id_part] = array(
                    'partname'          => $results[0]->partname,
                    'part_id'           => $part->id_part,
                    'type'              => $results[0]->type,
                    'totalQuantity'     => $part->quantity,
                    'qtyPurpose'        => array(array(
                        'purpose'   => $part->purpose,  
                        'qty'       => $part->quantity
                        ))
                    );
            }
            else{
                $partsToOrder[$part->id_part]['totalQuantity']    += $part->quantity;
                array_push( $partsToOrder[$part->id_part]['qtyPurpose'], array(
                    'purpose'   => $part->purpose,  
                    'qty'       => $part->quantity
                    )
                );
            }
        }
        $type  = array_column($partsToOrder, 'type');
        $partname = array_map('strtolower', array_column($partsToOrder, 'partname'));
        array_multisort($type, SORT_ASC, $partname, SORT_ASC, $partsToOrder);	
    }
    

    ?>
    <script type="text/javascript">

    function do_this(){
        var checkboxes = document.getElementsByClassName('cartCheckboxes');
        var button = document.getElementById('select-all');

        if(button.checked == ''){
            for (var i in checkboxes){
                checkboxes[i].checked = '';
            }
        }else{
            for (var i in checkboxes){
                checkboxes[i].checked = 'TRUE';
            }
        }
    }
    </script>
    
    <?php
    if(isset($_POST['btnAddToOrder'])){
        foreach ($_POST as $key => $value) {
            if(strpos($key, "cbItem") !== false)
                echo $key . ' -> ' . $value . '<br>';
        }
    }
    else{
        echo "<form action='' method='post' class='stockCSS'>";
        echo "<table style='margin:5%;width:90%'>";
        echo 	"<tr>";
        echo        "<th><input type='checkbox' name='select-all' id='select-all' onClick='do_this(this)'/></th>";
        echo 		"<th>Partname <span style='float: right;'>Info</span></th>";
        echo 		"<th>Type</th>";
        echo 		"<th>Description</th>";
        echo 		"<th>In stock</th>";
        echo 		"<th>To buy</th>";
        echo 		"<th>Per pkg</th>";
        echo 		"<th>Purpose</th>";
        echo 		"<th colspan='2'>Price</th>";
        echo 		"<th colspan='2'>Total Price</th>";
        echo 	"<tr>";

        $warehouseUrl = get_permalink( get_page_by_path( 'warehouse' ));
        //print_r($partsToOrder);

        foreach($partsToOrder as $id => $partToOrder){
            $table_name = $wpdb->prefix.'warehouse_parts';
            $results = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=".$partToOrder['part_id']);
            $part = $results[0];

            $qtyToOrder = $partToOrder['totalQuantity'] - $part->quantity;

            //print_r($part)."<br/>";
            if($qtyToOrder > 0)
                echo "<tr>";
            else
                echo "<tr>";
            echo   "<td><input class='cartCheckboxes' type='checkbox' id='cbItem".$part->id."' name='cbItem".$part->id."' value='".$part->id."'></td>";
            echo   "<td>";
            echo 	"<b><a href='".$warehouseUrl."?detailID=".$part->id."'>".$part->partname."</a>"."</b>";

            echo    "<span style='float: right;'>";
            if($part->pdf != "")
                echo " <a class='dashicons dashicons-pdf' href='".$part->pdf."'/> ";
            
            if($part->image != "")
                echo " <a class='dashicons dashicons-format-image' href='".$part->image."'/> ";

            if($part->multipart){
                $url = get_permalink( get_page_by_path( 'warehouse/list_of_multiparts' ));
                echo " <a class='dashicons dashicons-editor-ul' href='".$url."?part_id=".$part->id."'/> ";
            }
            echo    "</span>";
            echo 	"</td>";
            
            // type
            echo   "<td>";
            echo     $part->type;
            echo   "</td>";
            
            // description
            echo   "<td>";
            echo     $part->description;
            echo   "</td>";
            
            // in stock
            echo   "<td>";
            echo     $part->quantity;
            echo   "</td>";

            // qty
            echo   "<td>";
            if($qtyToOrder > 0){
                echo     "<font color='red'>".$qtyToOrder."</font>";
            }
            else{
                echo     0;
            }
            echo   "</td>";

            // quantity per box
            echo   "<td>";
            echo     $part->quantityPerBox;
            echo   "</td>";

            // purpose
            echo   "<td>";
            //print_r($partToOrder['qtyPurpose']);
            foreach($partToOrder['qtyPurpose'] as $qtys){
                echo "<b>".$qtys['qty']." pcs</b> for ".$qtys['purpose']."<br/>";
                /*if($qtyToOrder > $qtys['qty'])
                    echo "<b>".$qtys['qty']." pcs</b> for ".$qtys['purpose']."<br/>";
                else{
                    if($qtyToOrder > 0){
                        echo "<b>".$qtyToOrder." pcs</b> for ".$qtys['purpose']."<br/>";
                    }
                    else{
                        echo "<b>0 pcs</b> for ".$qtys['purpose']."<br/>";
                    }
                }
                $qtyToOrder -= $qtys['qty'];*/
            }
            echo   "</td>";

            // price per components
            echo   "<td>";
            echo     $part->price;
            echo   "</td>";

            echo   "<td>";
            echo     $part->currency;
            echo   "</td>";

            // total price per components
            echo   "<td>";
            if($qtyToOrder > 0)
                echo     round($part->price * $qtyToOrder, 3);
            else
                echo "0";
            echo   "</td>";

            echo   "<td>";
            echo     $part->currency;
            echo   "</td>";

            echo "</tr>";
        }
        echo "</table>";
        echo "<p style='margin:20px; width:90%;'><button style='float: right;' name='btnAddToOrder' type='submit'>Add to Order</button></p>";
        echo "</form>";
    }
}

add_shortcode('warehouse_cart', 'warehouse_cart');
