<?php

/**
 * Template Name: Export/Use goods
 *
 */
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';

function warehouse_export_goods_from_warehouse($atts) { 
	global $wpdb;
	$a = shortcode_atts( array(
		'type' => ""
	), $atts );

	$current_user = wp_get_current_user();
	
	if(!isset($_SESSION['export_step'])){
		$_SESSION['export_step'] = 1;
	}
	
	if(isset($_POST['btnCheck']) | isset($_SESSION['export_scannedParts'])){		
		if(isset($_POST['btnCheck']))
			$_SESSION['export_scannedParts'] = $_POST['scannedParts'];
		
		$decodedParts = array();
		$allValid = true;
		$receiptNumber = intval(getSettingsValue("EXPORT_RECEIPT_NUMBER"));
		$receiptTextNumber = str_pad($receiptNumber, 8, '0', STR_PAD_LEFT);

		foreach(preg_split("/((\r?\n)|(\r\n?))/", $_SESSION['export_scannedParts']) as $line){
			if($line == "")
				continue;
			
			$fields = explode (";", $line); 
			
			if(count($fields) >= 2){
				// get current quantity of the goods
				$table_name = $wpdb->prefix.'warehouse_parts';
				$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `barcode`='".$fields[0]."'");
				
				if(!empty($results)){
					$part = $results[0];
					
					if(count($fields) == 2){
						$x = array_search($part->barcode, array_column($decodedParts, 'name'));
						if($x !== false){
							$decodedParts[$x]['quantity'] += $fields[1];
						}
						else
							array_push($decodedParts, array('id'			=> $part->id, 
															'name' 			=> $part->barcode, 
															'partname' 		=> $part->partname, 
															'position' 		=> $part->position, 
															'posColour' 	=> $part->posColour, 
															'quantity' 		=> intval($fields[1]), 
															'stockQuantity' => $part->quantity, 
															'description'	=> $part->description, 
															'valid' 		=> 1
							));
					}
				}	
				// else{
				// 	$sql = "SELECT * FROM `stock_workers` WHERE `barcode` = '".$fields[0]."'";
				// 	$workers = mysqli_query($conn, $sql);
					
				// 	//echo "<script>console.log( 'Count 1: " . count($workers) . "' );</script>";
				// 	if($workers->num_rows == 1){
				// 		$worker = mysqli_fetch_assoc($workers);
				// 		//echo "<script>console.log( 'Debug Line 1: " . json_encode($worker) . "' );</script>";
						
						
				// 		$_SESSION['export_workerID'] = $fields[0];
				// 		if($worker['active'] == 0){
				// 			$allValid = false;
				// 			$recipient = $worker['name']." - <font color='red'><b>Není oprávněný přijímat</b></font>";
				// 		}
				// 		else{
				// 			$recipient = $worker['name'];
				// 		}
				// 	}
				// 	else{
				// 		$allValid = false;
				// 		if(count($fields) == 2)
				// 			array_push($decodedParts, array("name" => $fields[0], 'quantity' => $fields[1], 'valid' => 0));
				// 		else
				// 		array_push($decodedParts, array('text' => $line, 'valid'=>-1));
				// 	}
				// }
			}
			// else{
			// 	$conn->set_charset("utf8");
			// 	$sql = "SELECT * FROM `stock_workers` WHERE `barcode` = '".$fields[0]."'";
			// 	$workers = mysqli_query($conn, $sql);
				
			// 	//echo "<script>console.log( 'Count 1: " . count($workers) . "' );</script>";
			// 	if($workers->num_rows == 1){
			// 		$worker = mysqli_fetch_assoc($workers);
			// 		//echo "<script>console.log( 'Debug Line 1: " . json_encode($worker) . "' );</script>";
					
					
			// 		$_SESSION['export_workerID'] = $fields[0];
					
			// 		if(!$worker['active']){
			// 			$allValid = false;
			// 			$recipient = $worker['name']." - <font color='red'><b>Není oprávněný přijímat</b></font>";
			// 		}
			// 		else{
			// 			$recipient = $worker['name'];
			// 		}
			// 	}
			// 	else{
			// 		$allValid = false;
			// 		array_push($decodedParts, array('text' => $line, 'valid'=>-1));
			// 	}
			// }
			//echo "<script>console.log( 'Count 1: " . json_encode($decodedParts) . "' );</script>";
		} 
		// if(!isset($recipient)){
		// 	$allValid = false;
		// 	$recipient = "<font color='red'><b>Není zadán!</b></font>";
		// }
		
		$_SESSION['export_step'] = 2;
		$_SESSION['export_decodedParts'] = $decodedParts;
		$_SESSION['export_noIDs'] = count($decodedParts);
	}
	
	if(isset($_POST['btnSave'])){
		$parts = $_SESSION['export_decodedParts'];
		for($i=1; $i<=$_SESSION['export_noIDs']; ++$i){
			$text = 'quantity'.$i;
			$quantity = intval($_POST[$text]);
			$text = 'id'.$i;
			$partID = $_POST[$text];
			
			// get current quantity of the goods
			$table_name = $wpdb->prefix.'warehouse_parts';
			$results = $wpdb->get_results("SELECT * FROM ".$table_name." WHERE `id`=".$partID);
			$newQty = $results[0]->quantity - $quantity;

			// log the exported goods
			$receiptNumber = intval(getSettingsValue("EXPORT_RECEIPT_NUMBER"));
			$receiptTextNumber = str_pad($receiptNumber, 8, '0', STR_PAD_LEFT);

			$table_name = $wpdb->prefix.'warehouse_parts_log';
			$data  = array( 'partId'				=> $partID, 
							'quantityChange'		=> -$quantity, 
							'quantity'				=> $newQty,
							// 'price'					=> $price, 
							// 'currency'				=> $currency, 
							'warehousemanId'		=> $current_user->ID, 
							'warehousemanName'		=> $current_user->display_name, 
							'receipt'				=> "E".$receiptTextNumber
							// 'supplier'				=> 'export'
			);
			$wpdb->insert($table_name, $data);

			// update quantity of the goods
			$table_name = $wpdb->prefix.'warehouse_parts';
			$data = 	array('quantity' 	=> $newQty);			
			$where = 	array('id' 			=> $partID);
			$wpdb->update( $table_name, $data, $where);			
		}
		$receiptNumber = intval(getSettingsValue("EXPORT_RECEIPT_NUMBER"));
		updateSettingsValue("EXPORT_RECEIPT_NUMBER", $receiptNumber+1);

		$_SESSION['export_step'] = 3;
		unset($_SESSION['export_decodedParts']);
		unset($_SESSION['export_noIDs']);
		unset($_SESSION['export_scannedParts']);
	}
	
	if(isset($_POST['btnBack'])){
		--$_SESSION['export_step'];
	}
	
?>


<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<div class="wrap">
			<?php 
			switch($_SESSION['export_step']){
				case 1:
			?>
				
				<div align='center'>
					<button class="tablinkSel" id="scan" style="float: center;">Skenovat</button>
					<button class="tablink" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				
				<div>
					<form action="" method="post"  class="stockCSS">
						<div class="one_column">
							<label for="scannedParts" id="name_label">Naskenované součástky a jejich množství: <font color="red">*</font></label>
							<textarea id="scannedParts" rows="4" cols="50" name="scannedParts" required autofocus><?php if(isset($_SESSION['export_scannedParts'])) echo $_SESSION['export_scannedParts'] ?></textarea>
						</div>
						<div>
							<b>Format:</b> Barcode;Quantity
						</div>
						<div class="one_column">
							<button name="btnCheck" type="submit" style="float: right;">Zkontrolovat</button>
						</div>
					</form>
				</div>
			<?php
				break;
				case 2:
			?>
				<div align='center'>
					<button class="tablink" id="scan" style="float: center;">Skenovat</button>
					<button class="tablinkSel" id="check" style="float: center;">Kontrola</button>
					<button class="tablink" id="save" style="float: center;">Uložit</button>
				</div>
				
				<form action="" method="post"  class="stockCSS">
							
					<div class="row">
						<div class="two_columns">
							<div>
								<label>Výdejce:</label>
								<label class='subLabel'><?php echo $current_user->display_name ?></label>
							</div>
						</div>
						<div class="two_columns">
							<div>
								<label>Výdejka č: </label>
								<label class='subLabel'><?php echo "E".$receiptTextNumber ?></label>
							</div>
						</div>
					</div> 
					
					<table class="stockCSS">
					<tr class='headerRow'>
						<th>Název</th>
						<th class='thCenter'>Množství na skladě</th>
						<th>Vydávané množství</th>
						<th class='thCenter'>Umístění</th>
						<th>Popis</th>
					</tr>
					<?php
						$id = 0;
						foreach($decodedParts as $part){
							++$id;									
								if($part['valid'] == 0){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"2\">".$part['name']."</td>";
									echo "<td>".$part['quantity']."</td>";
									echo "<td>".$part['description']."</td>";
								}
								else if($part['valid'] == -1){
									echo "<tr style='background-color:#ffbba9'>";
									echo "<td colspan=\"6\">".$part['text']."</td>";
								}
								else{
									echo "<tr>";
									echo "<td>".$part['name']."<input style='display:none;' type='number' name='id".$id."' value='".intval($part['id'])."'\"></td>";
									echo "<td class='tdCenter'>".$part['stockQuantity']."</td>";
									echo "<td><input type='number' min='0' max='".$part['stockQuantity']."' name='quantity".$id."'  value='".intval($part['quantity'])."'></td>";
									echo "<td class='tdCenter'><b><font color='".$part['posColour']."'>".$part['position']."</font></b></td>";
									echo "<td>".$part['description']."</td>";
								}
								echo "</tr>";
							echo "</form>";
						}
						$_SESSION["export_rowCounts"] = $id;
					?>
					
					</table>

					<div class="one_column">
						<script>
							function removeRequired(form){
								$.each(form, function(key, value) {
									if ( value.hasAttribute("required")){
										value.removeAttribute("required");
									}
								});
							}
						</script>
						<?php if($allValid == true)
							echo "<button name='btnSave' type='submit' style='float: right;'>Vydat</button>";
						?>	
						<button name="btnBack" type="submit" style="float: left;" onClick="removeRequired(this.form)">Zpět</button>
					</div>
				</form>
			
			<?php
				break;
				case 3:
			?>
				<div align='center'>
					<button class="tablink" id="scan" style="float: center;">Skenovat</button>
					<button class="tablink" id="check" style="float: center;">Kontrola</button>
					<button class="tablinkSel" id="save" style="float: center;">Uložit</button>
				</div>
				
				<h2 class="stockCSS"><font color="green">Záznam uložen</font></h2>
				
				<form action="" method="post" class="stockCSS">
					<div class="one_column">
						<button name="btnNew" type="submit" style="float: right;">Nový záznam</button>
					</div>
				</form>
			<?php
				unset($_SESSION['export_step']);
				break;
			}
			?>

</div><!-- .wrap -->

<?php
}
add_shortcode('warehouse_export_goods_from_warehouse', 'warehouse_export_goods_from_warehouse'); 